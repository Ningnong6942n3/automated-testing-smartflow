*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

*** Variables ***

${Video}  https://shl-dev.brainergy.digital/assets/usermanualfile/%E0%B8%82%E0%B8%B1%E0%B9%89%E0%B8%99%E0%B8%95%E0%B8%AD%E0%B8%99%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%B4%E0%B8%94%E0%B8%95%E0%B8%B1%E0%B9%89%E0%B8%87%20forticlient%20vpn%20(%E0%B8%AA%E0%B8%B3%E0%B8%AB%E0%B8%A3%E0%B8%B1%E0%B8%9A%20ios)%20.MP4
${excel}   SmartFlow.xlsx

*** Keywords ***


Choose Tab User Manuals
    Wait Until Element Is Visible  //app-sidebar/nav/ul/li[4]  10s
    Click Element  //app-sidebar/nav/ul/li[4]
    wait until location is  ${UserManuals}  10s
    Wait Until Element Is Visible  //div[@class='user-manuals-wrapper']/ul/li[4]/div/button/span  10s
    ${UM}  Get Text  //div[@class='user-manuals-wrapper']/ul/li[1]/div/button/span
    Log to Console  ${UM}
    Click Element  //div[@class='user-manuals-wrapper']/ul/li[1]/div/button/i
    ${handle} =	Switch Window	NEW
    Location Should Be  ${Video} 
    ${Nowurl}=   Get Location
    wait until location is  ${Nowurl}  10s
    IF   '${Video}' == '${Nowurl}'
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT42-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(45,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser
     ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT42-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(45,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser
     END

*** Test Cases ***
UserManuals
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP 
    Choose Tab User Manuals
    
    
    

    
    
    