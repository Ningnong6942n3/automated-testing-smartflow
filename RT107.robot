*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1
${ErrorEmployees}  Please select Employee
*** Keywords ***




Edit - Add Employees
    Wait Until Element Is Visible  //div[@class='form-btn-wrapper']/button[3]  10s
    Click Button  //div[@class='form-btn-wrapper']/button[3]
    Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[1]/div/div  10s
    Click Element  //div[@class='modal-body']/form/div/div[1]/div/div
    Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[11]  10s
    Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[11]
    Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[2]/div/div/ng-select  10s
    Click Element  //div[@class='modal-body']/form/div/div[2]/div/div/ng-select
    Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]  10s
    Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]
    Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[3]/button[1]  10s
    Click Button  //div[@class='modal-body']/form/div/div[3]/button[1]
  

Error Massage 
    Wait Until Element Is Visible  //div[@class='invalid-feedback']/div  10s
    ${ErrorMassage}  Get Text  //div[@class='invalid-feedback']/div
    IF  '${ErrorMassage}' == '${ErrorEmployees}'
        Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT107-Pass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(110,13,'PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser
    ELSE  
        Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT107-Notpass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(110,13,'NOT PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser
    END
    


*** Test Cases ***
EditAddEmployees_Error2
    Open Website SMARTFLOW
    Requester - Auditor
    Click Btn Login 
    Input OTP And Click OTP 
    Click Nav LOA - Edit (Tab)
    Edit - Add Employees
    Error Massage 
    
    
    

    
    
    