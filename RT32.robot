*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

*** Variables ***
${excel}   SmartFlow.xlsx
${text}  Pass 
${NUMBER}  1
*** Keywords ***



Filter Doc No.
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Click Button  //div[@class='col-sm-12 search-box-section']/button[1]
    Wait Until Element Is Visible  //tbody/tr[1]/td[7]/div/button  10s
    ${Data2}  Get Text  //tbody/tr[1]/td[7]/div/button
    Log To Console   ${Data2}
    Input text  //div[@class='card table-filter']/div/div[4]/input  ${Data2}
    ${Data}  Get Text  //tbody/tr[1]/td[7]/div/button
    Log To Console  ${Data}
    Should Contain  ${Data2}  ${Data}
    ${CheckShowData} =	Get Element Count  //tbody/tr
    Log To Console  ${CheckShowData}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad} 
    ${Show}  Get Text  //tbody/tr/td
    Log to Console   ${Show}
    IF    ${CheckShowData} == ${NUMBER}
       ${Show}  Get Text  //tbody/tr[1]/td[7]/div/button     
                    IF   '${Data}' == '${Show}'
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT32-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(35,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT32-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(35,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser
                    END
    ELSE IF    ${CheckShowData} > ${NUMBER}
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT32-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(35,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser 
    END 


*** Test Cases ***
Filter-Status1
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Filter Doc No.
   
    
   
    