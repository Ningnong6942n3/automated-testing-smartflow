*** Settings ***
Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot
*** Variables ***
${excel}   SmartFlow.xlsx
*** Keywords ***


Check Error Massage 
    ${ErrorMessage1}  Get Text  //div[@class="invalid-feedback"]/div[text()="Please select reason"]
    ${ErrorMessage2}  Get Text  //div[@class="invalid-feedback"]/div[text()="Please type subject"]
    ${ErrorMessage3}  Get Text  //div[@class="invalid-feedback"]/div[text()="Please Select To"]
    Sleep  5
    IF    '${ErrorMessage1}' == 'Please select reason'
         IF    '${ErrorMessage2}' == 'Please type subject'
              IF    '${ErrorMessage3}' == 'Please Select To'
                      Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT11-Pass.png
                      ${wb}      Load Workbook     ${CURDIR}/${excel}
                      Log to Console   ${wb}
                      ${ws}      Set Variable  ${wb['Sheet1']}
                      Log To Console   ${ws}
                      Evaluate   $ws.cell(14,13,'PASS')
                      Evaluate   $wb.save('${excel}')
                      Close All Browsers
                 ELSE
                      Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT11-Notpass.png
                      ${wb}      Load Workbook     ${CURDIR}/${excel}
                      Log to Console   ${wb}
                      ${ws}      Set Variable  ${wb['Sheet1']}
                      Log To Console   ${ws}
                      Evaluate   $ws.cell(14,13,'NOT PASS')
                      Evaluate   $wb.save('${excel}')
                      Close All Browsers
                 END
         ELSE
              Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT11-Notpass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(14,13,'NOT PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
        END
    ELSE
              Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT11-Notpass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(14,13,'NOT PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
    END
*** Test Cases ***
Document Details-False1
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Click Btn Create Document 
    Click Btn Next
    Check Error Massage 

    