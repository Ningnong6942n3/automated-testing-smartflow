*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***

${excel}   SmartFlow.xlsx
${NUMBER}  1
${Exit}

*** Keywords ***



Click Nav LOA - Create LOA
    Wait Until Element Is Visible  //ul[@class='nav']/li[4]  10s
    Click Element  //ul[@class='nav']/li[4]
    Wait Until Element Is Visible  //ul[@class='nav']/li[4]/div/ul/li[2]/a  10s
    Click Element  //ul[@class='nav']/li[4]/div/ul/li[2]/a
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${LOA_Code}  Get Text  //tbody/tr[1]/td[2]
    Set global variable  ${LOA_Code}

Click btn Filter
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button[1]  10s
    Sleep  5
    Click Button  //div[@class='col-sm-12 search-box-section']/button[1]
    Wait Until Element Is Visible  //div[@class='card table-filter']/div/div[1]/input  10s
    Input Text  //div[@class='card table-filter']/div/div[1]/input  ${LOA_Code}
    ${LOA_CodeCH}  Get Text  //tbody/tr[1]/td[2]
    ${count} =	Get Element Count  //tbody/tr
    Log To Console  ${count}
    IF  '${count}' == '1'
        Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT96-Pass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(99,13,'PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser
                IF  '${LOA_Code}' == '${LOA_CodeCH}'
                    Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT96-Pass.png
                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                    Log to Console   ${wb}
                    ${ws}      Set Variable  ${wb['Sheet1']}
                    Log To Console   ${ws}
                    Evaluate   $ws.cell(99,13,'PASS')
                    Evaluate   $wb.save('${excel}')
                    Close Browser
                ELSE
                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT96-Notpass.png
                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                    Log to Console   ${wb}
                    ${ws}      Set Variable  ${wb['Sheet1']}
                    Log To Console   ${ws}
                    Evaluate   $ws.cell(99,13,'NOT PASS')
                    Evaluate   $wb.save('${excel}')
                    Close Browser
                END
    ELSE
        Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT96-Notpass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(99,13,'NOT PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser
    END
   



*** Test Cases ***
Designer Approvers - Filter_LOACode
    Open Website SMARTFLOW
    Requester - Auditor
    Click Btn Login 
    Input OTP And Click OTP
    Click Nav LOA - Create LOA
    Click btn Filter
    
  
    
  
    
    
   
    
    

    
    
    