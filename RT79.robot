*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot
Resource    functions/Create LOA.robot

*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1
${PopUpCancle}  Are you sure to cancel?
${Employees_NA}
${Pocition_Approver1}  Department Director
${Pocition_Approver2}  Operation Manager
${LOACode}
${LOANameError}  Please insert LOA Name
${BusinessUnitError}  Please select BU
${LOADescriptionError}  Please insert LOA Description
${ManageApproverError}  Please select Approver
*** Keywords ***



Click btn Claer - Check Error Messages 
    Wait Until Element Is Visible  //div[@class='form-action-wrapper']/button[4]  10s
    Click Button  //div[@class='form-action-wrapper']/button[4]
    Wait Until Element Is Visible  //div[@class='form-action-wrapper']/button[2]  10s
    Click Button  //div[@class='form-action-wrapper']/button[2]
    ${LOAName_Error}  Get Text  //div[@class='form-content-wrapper']/div/div[1]/div/div
    ${BusinessUnit_Error}  Get Text  //div[@class='form-content-wrapper']/div/div[2]/div/div  
    ${LOADescription_Error}  Get Text  //div[@class='form-content-wrapper']/div/div[3]/div/div
    ${ManageApprover_Error}  Get Text  //div[@class='form-btn-wrapper']/div/div/div/div
    IF   '${LOAName_Error}' == '${LOANameError}'
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT79-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(82,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                        
                              IF   '${BusinessUnit_Error}' == '${BusinessUnitError}'
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT79-Pass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(82,13,'PASS')
                                    Evaluate   $wb.save('${excel}')

                                            IF   '${LOADescription_Error}' == '${LOADescriptionError}'
                                                Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT79-Pass.png
                                                ${wb}      Load Workbook     ${CURDIR}/${excel}
                                                Log to Console   ${wb}
                                                ${ws}      Set Variable  ${wb['Sheet1']}
                                                Log To Console   ${ws}
                                                Evaluate   $ws.cell(82,13,'PASS')
                                                Evaluate   $wb.save('${excel}')

                                                        IF   '${ManageApprover_Error}' == '${ManageApproverError}'
                                                                Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT79-Pass.png
                                                                ${wb}      Load Workbook     ${CURDIR}/${excel}
                                                                Log to Console   ${wb}
                                                                ${ws}      Set Variable  ${wb['Sheet1']}
                                                                Log To Console   ${ws}
                                                                Evaluate   $ws.cell(82,13,'PASS')
                                                                Evaluate   $wb.save('${excel}')
                                                                Close Browser
                                                        ELSE
                                                                Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT79-Notpass.png
                                                                ${wb}      Load Workbook     ${CURDIR}/${excel}
                                                                Log to Console   ${wb}
                                                                ${ws}      Set Variable  ${wb['Sheet1']}
                                                                Log To Console   ${ws}
                                                                Evaluate   $ws.cell(82,13,'NOT PASS')
                                                                Evaluate   $wb.save('${excel}')
                                                                Close Browser
                                                        END
                                            ELSE
                                                Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT79-Notpass.png
                                                ${wb}      Load Workbook     ${CURDIR}/${excel}
                                                Log to Console   ${wb}
                                                ${ws}      Set Variable  ${wb['Sheet1']}
                                                Log To Console   ${ws}
                                                Evaluate   $ws.cell(82,13,'NOT PASS')
                                                Evaluate   $wb.save('${excel}')
                                                Close Browser
                                            END
                              ELSE
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT79-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(82,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                                    Close Browser
                              END
    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT79-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(82,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser
    END


*** Test Cases ***
Designer Approvers - CreateLOA_Claer
    Open Website SMARTFLOW
    Requester - Auditor
    Click Btn Login 
    Input OTP And Click OTP
    Click Nav LOA - Create LOA
    Create LOA - Tnput Data
    Click btn Claer - Check Error Messages 
   
    
   
    
    
    
    
  
    
    
   
    
    

    
    
    