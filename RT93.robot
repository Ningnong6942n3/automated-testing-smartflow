*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***

${excel}   SmartFlow.xlsx
${NUMBER}  1
${Exit}


*** Keywords ***





Click btn Filter
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button[1]  10s
    Sleep  5
    Click Button  //div[@class='col-sm-12 search-box-section']/button[1]
    Wait Until Element Is Visible  //div[@class='card table-filter']/div/div[3]/ng-select  10s
    Click Element  //div[@class='card table-filter']/div/div[3]/ng-select
    Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]/span  10s
    ${Bu_LOA}  Get Text  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]/span 
    Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]  10s
    Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    IF    '${Show}' == 'No matching records found'
        Capture Page Screenshot   E:/TEstAll/Screenshor_Notpass/RT93-Notpass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(96,13,'NOT PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser 
    ELSE
    ${CountShowitems} =	Get Element Count  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a
    ${countAll}  Get Text  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[${CountShowitems}]
    ${CheckShowData} =	Get Element Count  //tbody/tr
    Log To Console  ${CheckShowData}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    FOR    ${z}    IN RANGE    ${countAll}
    ${Zn}  Evaluate  ${z}+1
    FOR    ${i}    IN RANGE    ${CheckShowData}
     ${n}  Evaluate  ${i}+1
     IF    ${CheckShowData} == ${NUMBER}
       ${Show}  Get Text  //tbody/tr[1]/td[4]
                                IF  '${Show}' == '${Bu_LOA}'
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT93-Pass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(96,13,'PASS')
                                    Evaluate   $wb.save('${excel}')
                                    Close Browser
                                ELSE
                                    Capture Page Screenshot  E:/TEstAll/Screenshor_Notpass/RT93-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(96,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                                END
                    
    ELSE IF  ${CheckShowData} > ${NUMBER}
                                ${Show}  Get Text  //tbody/tr[${n}]/td[4]
                                IF  '${Show}' == '${Bu_LOA}'
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT93-Pass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(96,13,'PASS')
                                    Evaluate   $wb.save('${excel}')
                                    
                                ELSE
                                    Capture Page Screenshot  E:/TEstAll/Screenshor_Notpass/RT93-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(96,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                                    ${Exit}  Set Variable  Exit
                                    Set Global Variable  ${Exit}
                                    Exit For Loop IF  "${Exit}" == "Exit" 
                                END
                        Exit For Loop IF  "${Exit}" == "Exit"  
                    END
            
          Exit For Loop IF  "${Exit}" == "Exit"  
    END 
        Exit For Loop IF  "${Exit}" == "Exit"  
        Run Keyword and Ignore Error   Click Element  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[text()='${Zn}']
    END 
    Close Browser
    END



*** Test Cases ***
Designer Approvers - Filter_Bu01
    Open Website SMARTFLOW
    Requester - Auditor
    Click Btn Login 
    Input OTP And Click OTP
    Click Nav LOA 
    Click btn Filter
   
  
    
  
    
    
   
    
    

    
    
    