*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1

*** Keywords ***



    

LOA Designer Users - Filter BU 
    wait until location is  https://shl-dev.brainergy.digital/work-space/loa/designer-users  10s
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button  10s
    Sleep  5
    Click Button  //div[@class='col-sm-12 search-box-section']/button
    ${CheckFilter}  Run Keyword And Return Status    Wait Until Element Is Visible  //div[@class='card table-filter']/div/div/ng-select  10s 
    Run Keyword If  '${CheckFilter}' != 'True'  Click Button  //div[@class='col-sm-12 search-box-section']/button
    Click Element  //div[@class='card table-filter']/div/div/ng-select
    ${NameBu}  Get Text  //ng-dropdown-panel/div/div/div[1]
    Wait Until Element Is Visible  //ng-dropdown-panel/div/div/div[1]  10s
    Click Element  //ng-dropdown-panel/div/div/div[1]
    Log to console  ${NameBu}
    ${count} =	Get Element Count  //div[@class='card-body card-table-body']/div/div/div/table/tbody/tr
    Log To Console  ${count}
    IF  ${count} == ${NUMBER}
                ${NumberBu}  Get Text  //table/tbody/tr/td[1]
                Should Contain  1  ${NumberBu} 
                ${Name(Bu)Show}  Get Text  //table/tbody/tr/td[2]
                Should Contain  ${NameBu}  ${Name(Bu)Show}
                Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT109-Pass.png
                ${wb}      Load Workbook     ${CURDIR}/${excel}
                Log to Console   ${wb}
                ${ws}      Set Variable  ${wb['Sheet1']}
                Log To Console   ${ws}
                Evaluate   $ws.cell(112,13,'PASS')
                Evaluate   $wb.save('${excel}')
                Close Browser
    ELSE  
                Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT109-Notpass.png
                ${wb}      Load Workbook     ${CURDIR}/${excel}
                Log to Console   ${wb}
                ${ws}      Set Variable  ${wb['Sheet1']}
                Log To Console   ${ws}
                Evaluate   $ws.cell(112,13,'NOT PASS')
                Evaluate   $wb.save('${excel}')
                Close Browser
     END
  



*** Test Cases ***
LOA Designer Users - Filter01
    Open Website SMARTFLOW
    Requester - Auditor
    Click Btn Login 
    Input OTP And Click OTP
    Click Nav LOA - LOA Designer Users
    LOA Designer Users - Filter BU 
    
    
    

    
    
    