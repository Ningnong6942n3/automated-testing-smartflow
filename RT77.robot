*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot


*** Variables ***
${excel}   SmartFlow.xlsx
${Data}
${NUMBER}  1
*** Keywords ***


Search Document
    wait until location is  https://shl-dev.brainergy.digital/work-space/documents/waiting-for-my-approval/tab/waiting-for-my-approval  10s
     Wait Until Element Is Visible  //div[@class]/div[@class='h-100']/app-waiting-for-my-approval/ul/li[2]  10s
    Click Element  //div[@class]/div[@class='h-100']/app-waiting-for-my-approval/ul/li[2]
    wait until location is  https://shl-dev.brainergy.digital/work-space/documents/waiting-for-my-approval/tab/my-approval  10s
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button[1]  10s
    Sleep  7
    Click Button  //div[@class='col-sm-12 search-box-section']/button[1]
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Data2}  Get Text  //tbody/tr[1]/td[7]/div/button
    Log To Console   ${Data2}
    Wait Until Element Is Visible  //div[@class='card table-filter']/div/div[4]/input  10s
    Input text  //div[@class='card table-filter']/div/div[4]/input  ${Data2}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    IF    '${Show}' == 'No matching records found'
        Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT77-Pass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(80,13,'PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser 
    ELSE
    ${CountShowitems} =	Get Element Count  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a
    ${countAll}  Get Text  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[${CountShowitems}]
    ${CheckShowData}=	Get Element Count  //tbody/tr
    Log To Console  ${CheckShowData}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
     IF    ${CheckShowData} == ${NUMBER}
           ${Show}  Get Text  //tbody/tr[1]/td[7]/div/button   
                    IF   '${Show}' == '${Data2}'
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT77-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(80,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT77-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(80,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser
                    END
    ELSE IF    ${CheckShowData} > ${NUMBER}
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT77-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(80,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser
    END
    END
    
*** Test Cases ***
MY APPROVAL - Filter_DocNo
    Open Website SMARTFLOW
    Approver Login (MK)
    Click Btn Login 
    Input OTP And Click OTP
    Search Document
    
    
    
   
    