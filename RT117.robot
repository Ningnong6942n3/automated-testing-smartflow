*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***

${excel}   SmartFlow.xlsx
${NUMBER}  1
${Approve}  Approve
${TextReject}  ทดสอบการ ReJect (LOA)


*** Keywords ***



Quick Approver - Approve
    Wait Until Element Is Visible  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[text()=3]  10s
    Click Element  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[text()=3]
    Wait Until Element Is Visible  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[text()=3]  10s
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Code_LOA}  Get Text  //tbody/tr[3]/td[1]/button
    Set global variable  ${Code_LOA}
    Log To Console  ${Code_LOA}
    ${Name_LOA}  Get Text  //tbody/tr[3]/td[2]/button
    Set global variable  ${Name_LOA} 
    Log To Console  ${Name_LOA} 
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Click Button  //tbody/tr[3]/td[7]/button
    Wait Until Element Is Visible  //div[@class='modal-body']/div/div/h5  10s
    ${Details}  Get Text  //div[@class='modal-body']/div/div/h5
    Log To Console  ${Details}
    Should Contain  LOA Details  ${Details} 
    ${DetailsName}  Get Text  //div[@class='info-wrapper']/div[3]/div[2]/p
    Should Contain  ${Name_LOA}  ${DetailsName}
    Wait Until Element Is Visible  //div[@class='info-wrapper']/div[2]/div[2]/p  10s
    ${DetailsLOACode}  Get Text  //div[@class='info-wrapper']/div[2]/div[2]/p
    Should Contain  ${Code_LOA}  ${DetailsLOACode}
    
Click btn Approve
    Wait Until Element Is Visible  //div[@class='modal-body']/div[2]/button[5]  10s
    Click Button  //div[@class='modal-body']/div[2]/button[5]
    Wait Until Page Contains  Your LOA was approved  10s
    wait until location is  https://shl-dev.brainergy.digital/work-space/documents/waiting-for-my-approval/tab/waiting-for-my-approval-loa  10s
    Sleep  5



Click btn Filter - Check Data
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button[1]  10s
    Sleep  5
    Click Button  //div[@class='col-sm-12 search-box-section']/button[1]
    Wait Until Element Is Visible  //div[@class='card table-filter']/div/div[1]/input  10s
    Input Text  //div[@class='card table-filter']/div/div[1]/input  ${Code_LOA}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${count} =	Get Element Count  //tbody/tr
    Log To Console  ${count}
          IF    ${count} == ${NUMBER}
            ${Show}  Get Text  //tbody/tr[1]/td[2]
            Should Contain    ${Code_LOA}     ${Show}  
            ${Status}  Get text  //tbody/tr[1]/td[6]/span
            Should Contain   Approved	 ${Status}
            Click Button  //tbody/tr[1]/td[7]/div/button
            Wait Until Element Is Visible  //div[@class='info-wrapper']/div[1]/div[2]/span  10s
            ${MassageApprove}  Get Text  //div[@class='info-wrapper']/div[1]/div[2]/span
            Should Contain  ${MassageApprove}  Approved
            Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT117-Pass.png
            ${wb}      Load Workbook     ${CURDIR}/${excel}
            Log to Console   ${wb}
            ${ws}      Set Variable  ${wb['Sheet1']}
            Log To Console   ${ws}
            Evaluate   $ws.cell(120,13,'PASS')
            Evaluate   $wb.save('${excel}')
            Close Browser
          ELSE 
            Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT117-Notpass.png
            ${wb}      Load Workbook     ${CURDIR}/${excel}
            Log to Console   ${wb}
            ${ws}      Set Variable  ${wb['Sheet1']}
            Log To Console   ${ws}
            Evaluate   $ws.cell(120,13,'NOT PASS')
            Evaluate   $wb.save('${excel}')
            Close Browser
          END
    
   



*** Test Cases ***
ApproveLOA - QuickApprover_Approve
    Open Website SMARTFLOW
    Approve - Audit_director
    Click Btn Login 
    Input OTP And Click OTP
    Choose Tab LOA 
    Quick Approver - Approve
    Click btn Approve
    Sign Out 
    Requester - Auditor
    Click Btn Login 
    Input OTP And Click OTP
    Click Nav LOA - Quick Approver 
    Click btn Filter - Check Data
   
    

   
    
  
    
    
   
    
    

    
    
    