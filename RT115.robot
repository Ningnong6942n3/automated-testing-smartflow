*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***

${excel}   SmartFlow.xlsx
${NUMBER}  1


*** Keywords ***



    
Click btn Filter
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${LOA_Code}  Get Text  //tbody/tr[1]/td[1]
    Set global variable  ${LOA_Code}
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button[1]  10s
    Click Button  //div[@class='col-sm-12 search-box-section']/button[1]
    ${CheckFilter}  Run Keyword And Return Status   Wait Until Element Is Visible  //div[@class='card table-filter']/div/div[1]/input  10s
    Run Keyword If  '${CheckFilter}' != 'True'  Click Button  //div[@class='col-sm-12 search-box-section']/button[1]
    Input Text  //div[@class='card table-filter']/div/div[1]/input  ${LOA_Code}
    ${count} =	Get Element Count  //tbody/tr
    Log To Console  ${count}
    IF  ${NUMBER} == ${count}
            IF  ${NUMBER} == ${count}
                ${LOA_CodeCH}  Get Text  //tbody/tr[1]/td[1]
                Should Contain  ${LOA_Code}  ${LOA_CodeCH}
                Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT115-Pass.png
                ${wb}      Load Workbook     ${CURDIR}/${excel}
                Log to Console   ${wb}
                ${ws}      Set Variable  ${wb['Sheet1']}
                Log To Console   ${ws}
                Evaluate   $ws.cell(118,13,'PASS')
                Evaluate   $wb.save('${excel}')
                Close Browser
            ELSE
                Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT115-Notpass.png
                ${wb}      Load Workbook     ${CURDIR}/${excel}
                Log to Console   ${wb}
                ${ws}      Set Variable  ${wb['Sheet1']}
                Log To Console   ${ws}
                Evaluate   $ws.cell(118,13,'NOT PASS')
                Evaluate   $wb.save('${excel}')
                Close Browser
            END
    ELSE  
        Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT115-Notpass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(118,13,'NOT PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser
    END


*** Test Cases ***
ApproveLOA - Filter_LOACode
    Open Website SMARTFLOW
    Approve - Audit_director
    Click Btn Login 
    Input OTP And Click OTP
    Choose Tab LOA 
    Click btn Filter
   
    
  
    
  
    
    
   
    
    

    
    
    