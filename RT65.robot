*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot
Resource    functions/Quick Approve.robot


*** Variables ***
${excel}   SmartFlow.xlsx
${text}  Pass 
${NUMBER}  1
${TimeCreateApprove}
*** Keywords ***


 
Click Btn Approve
    Click Button  //div[@class='text-center mt-1']/button[4]
    Wait Until Page Contains  Your document was approved and sent to the next level.  10s
    Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT65-Pass.png
    ${wb}      Load Workbook     ${CURDIR}/${excel}
    Log to Console   ${wb}
    ${ws}      Set Variable  ${wb['Sheet1']}
    Log To Console   ${ws}
    Evaluate   $ws.cell(68,13,'PASS')
    Evaluate   $wb.save('${excel}')
    Close Browser
    


Check Status On Process
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Click Button  //div[@class='col-sm-12 search-box-section']/button[2]
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/input  10s
    Input Text  //div[@class='col-sm-12 search-box-section']/input  ${NameDocument}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    IF    '${Show}' == 'No matching records found'
        Capture Page Screenshot   E:/Smartflow robot/Screenshor_Notpass/RT65-Notpass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(68,13,'NOT PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser 
    ELSE
    ${CountShowitems} =	Get Element Count  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a
    ${countAll}  Get Text  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[${CountShowitems}]
    ${CheckShowData} =	Get Element Count  //tbody/tr
    Log To Console  ${CheckShowData}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    FOR    ${z}    IN RANGE    ${countAll}
    ${Zn}  Evaluate  ${z}+1
    FOR    ${i}    IN RANGE    ${CheckShowData}
     ${n}  Evaluate  ${i}+1
     IF    ${CheckShowData} == ${NUMBER}
                                ${Show}  Get Text  //tbody/tr[1]/td[1]/button/p
                                IF  '${Show}' == '${NameDocument}'
                                    ${Show}  Get Text  //tbody/tr[1]/td[9]
                                    IF    '${Show}' == 'On Process'
                                        Click Button  //tbody/tr[1]/td[10]/button
                                        Wait Until Element Is Visible  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/p/label/span[1]  10s
                                        ${Status}  Get Text  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/p/label/span[1]
                                        Should Contain       On Process       ${Status}
                                        Wait Until Element Is Visible  //div[@class='right-wrapper d-sm-none d-md-block d-none']/div/property-nav/div/ul[@class='nav-pills nav-vertical nav flex-column']/li[3]  10s
                                        Click Element  //div[@class='right-wrapper d-sm-none d-md-block d-none']/div/property-nav/div/ul[@class='nav-pills nav-vertical nav flex-column']/li[3]
                                        ${CheckShowData} =	Get Element Count  //div[@class='right-wrapper d-sm-none d-md-block d-none']/div/property-nav/div/div/div/property-audit-trail/div/div[2]/ul/li
                                        Log To Console  ${CheckShowData}
                                        FOR    ${i}    IN RANGE    ${CheckShowData}
                                        IF    ${CheckShowData} == ${NUMBER}
                                        Wait Until Page Contains  Waritsara Sonchan  10s
                                        Wait Until Page Contains  Create this document  10s
                                        ELSE IF    ${CheckShowData} > ${NUMBER}
                                        Wait Until Page Contains  Waritsara Sonchan  10s
                                        Wait Until Page Contains  Create this document  10s
                                        Wait Until Page Contains  Darin Pongsupat  10s
                                        Wait Until Page Contains  Approve this document  10s
                                        Wait Until Page Contains  ${TimeCreateApprove}  10s
                                        END
                                        END
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT65-Pass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(68,13,'PASS')
                                    Evaluate   $wb.save('${excel}')
                                    ${Exit}  Set Variable  Exit
                                    Set Global Variable  ${Exit}
                                    Exit For Loop IF  "${Exit}" == "Exit" 
                                        Close Browser
                                    ELSE 
                                        Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT65-Notpass.png
                                        ${wb}      Load Workbook     ${CURDIR}/${excel}
                                        Log to Console   ${wb}
                                        ${ws}      Set Variable  ${wb['Sheet1']}
                                        Log To Console   ${ws}
                                        Evaluate   $ws.cell(68,13,'NOT PASS')
                                        Evaluate   $wb.save('${excel}') 
                                        Close Browser
                                    END
                                ELSE
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT65-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(68,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                                END
    ELSE IF    ${CheckShowData} > ${NUMBER}
                                ${Show}  Get Text  //tbody/tr[${n}]/td[1]/button/p
                                IF  '${Show}' == '${NameDocument}'
                                     ${Show}  Get Text  //tbody/tr[${n}]/td[9]
                                     IF    '${Show}' == 'On Process'
                                        Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
                                        Click Button  //tbody/tr[${n}]/td[10]/button
                                        Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
                                        Click Element  //div[@class='right-wrapper d-sm-none d-md-block d-none']/div/property-nav/div/ul[@class='nav-pills nav-vertical nav flex-column']/li[3]
                                        ${CheckShowData} =	Get Element Count  //div[@class='right-wrapper d-sm-none d-md-block d-none']/div/property-nav/div/div/div/property-audit-trail/div/div[2]/ul/li
                                        Log To Console  ${CheckShowData}
                                        FOR    ${i}    IN RANGE    ${CheckShowData}
                                        IF    ${CheckShowData} == ${NUMBER}
                                        Wait Until Page Contains  Waritsara Sonchan  10s
                                        Wait Until Page Contains  Create this document  10s
                                        ELSE IF    ${CheckShowData} > ${NUMBER}
                                        Wait Until Page Contains  Waritsara Sonchan  10s
                                        Wait Until Page Contains  Create this document  10s
                                        Wait Until Page Contains  Darin Pongsupat  10s
                                        Wait Until Page Contains  Approve this document  10s
                                        Wait Until Page Contains  ${TimeCreateApprove}  10s
                                        END
                                        END
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT65-Pass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(68,13,'PASS')
                                    Evaluate   $wb.save('${excel}')
                                    ${Exit}  Set Variable  Exit
                                    Set Global Variable  ${Exit}
                                    Exit For Loop IF  "${Exit}" == "Exit"  
                                ELSE
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT65-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(68,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                            END
                        Exit For Loop IF  "${Exit}" == "Exit"  
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT65-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(68,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          
                    END
             Exit For Loop IF  "${Exit}" == "Exit"  
         END
          Exit For Loop IF  "${Exit}" == "Exit"  
    END 
        Exit For Loop IF  "${Exit}" == "Exit"  
        Run Keyword and Ignore Error   Click Element  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[text()='${Zn}']
    END 
    Close Browser
    END


*** Test Cases ***
WAITING FOR MY APPROVAL_QuickApprove-Apporve
    Open Website SMARTFLOW
    Approver Login (DR)
    Click Btn Login 
    Input OTP And Click OTP
    Click btn Quick Approve
    Click Btn Approve
    
    

    
    
    