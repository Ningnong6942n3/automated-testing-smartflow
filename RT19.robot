*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

*** Variables ***
${excel}   SmartFlow.xlsx
${Document}  ทดสอบการสร้างเอกสารทดสอบ
*** Keywords ***



Input Subject (Random Numbers)
    ${Random Numbers}  generate random string  3  [NUMBERS]
    Set Global Variable  ${Random Numbers}
    Input Text  //div[@class="col-sm-9"]/input  ${Document}(${Random Numbers})

Input Article
    Page Should Not Contain  Delete
    Input Text  //quill-editor/div/div[@class='ql-editor ql-blank']  BRAINERGY เบรนเนอร์จี เริ่มก่อตั้งเมื่อเดือนธันวาคม 2561 โดยมุ่งหวังเพื่อช่วยให้องค์การของประเทศไทยสามารถใช้ประโยชน์จากเทคโนโลยีดิจิตอลในการทำงานและขยายโอกาสในการทำธุรกิจผ่านระบบดิจิตอลด้วย BRAINERGY เป็นบริษัทในเครือของเบญจจินดาผู้พัฒนาเทคโนโลยีทางด้านการสื่อสารไทยมามากกว่า 50 ปี ทำให้ BRAINERGY สามารถนำศักยภาพเทคโนโลยีดิจิตอลมาตอบโจทย์และเข้าใจการทำงานของธุรกิจไทยได้อย่างเหมาะสม
Click btn Send
    Click Button  //div[@class='text-center p-3']/button[4]
    Wait Until Page Contains  Your document was created and sent to the approver.  10s
    Wait Until Location Is  ${MAINWEB}  10s
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Wait Until Page Contains  ${Document}(${Random Numbers})  10s
    ${NameDocument}  Get Text  //tbody/tr[1]/td[1]/button 
    IF    '${Document}(${Random Numbers})' == '${NameDocument}'
              Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT19-Pass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(22,13,'PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
          ELSE
              Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT19-Notpass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(22,13,'NOT PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
    END

*** Test Cases ***
CreateDocument-Send
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Click Btn Create Document 
    Choose For
    Input Subject (Random Numbers)
    Choose LOA 
    Choose Approval 1
    Choose Approval 2
    Click Btn Next
    Input Article
    Click btn Send
    
   
    
  
    