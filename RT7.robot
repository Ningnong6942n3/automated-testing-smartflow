*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot

*** Variables ***
${excel}   SmartFlow.xlsx

*** Keywords ***

Check Error Massage 
    Wait Until Page Contains  * Username is required!
    ${ErrorMessage}  Get Text  //div[@class='errors-wrapper']/h6
    IF    '${ErrorMessage}' == '* Username is required!'
              Page Should Contain  * Username is required!
              Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT7-Pass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(10,13,'PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
          ELSE
              Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT7-Notpass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(10,13,'NOT PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
    END
    
*** Test Cases ***
Test Login Password Entered Incorrectly.
    Open Website SMARTFLOW
    Requestor Login Password Entered Incorrectly And Click Btn Login (Requestor)
    Check Error Massage 
  