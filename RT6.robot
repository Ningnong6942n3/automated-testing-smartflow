*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot

*** Variables ***
${excel}   SmartFlow.xlsx

*** Keywords ***

Check Error Massage 
    Wait Until Page Contains  * Password is required!
    ${ErrorMessage}  Get Text  //div[@class='errors-wrapper']/h6
    IF    '${ErrorMessage}' == '* Password is required!'
              Page Should Contain  * Password is required!
              Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT6-Pass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(9,13,'PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
          ELSE
              Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT6-Notpass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(9,13,'NOT PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
    END

*** Test Cases ***

Test Login Password Entered Incorrectly.
    Open Website SMARTFLOW
    Requestor Login Username Entered Incorrectly And Click Btn Login (Requestor)
    Check Error Massage 
    