*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot

*** Variables ***
${excel}   SmartFlow.xlsx

*** Keywords ***


Click Btn OTP 
    Click Button  //div[@class='otp-footer']/button
    ${Nowurl}=   Get Location 
    IF    '${Nowurl}' == '${OTP}'
              Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT8-Pass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(11,13,'PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
          ELSE
              Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT8-Notpass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(11,13,'NOT PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
    END
    
*** Test Cases ***
Test Login Not Input OTP
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Click Btn OTP 
    