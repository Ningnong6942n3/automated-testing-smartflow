*** Settings ***
Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot
Resource    functions/Quick Approve.robot
Resource    functions/Create LOA.robot

*** Variables ***
${URL}  https://shl-dev.brainergy.digital/accounts/login
${OTP}  https://shl-dev.brainergy.digital/accounts/otp-confirm
${MAINWEB}  https://shl-dev.brainergy.digital/work-space/documents/my-document/tab/my-document
${CreateDoc}  https://shl-dev.brainergy.digital/work-space/documents/my-document/create/content
${BROWSER}  Chrome
${excel}   SmartFlow.xlsx
${text}  Pass 
${Document}  ทดสอบการสร้างเอกสาร
${fileJPG}  E:/Smartflow robot/file/fileJPG.jpg
${filepdf}  E:/Smartflow robot/file/filepdf.pdf
${filepng}  E:/Smartflow robot/file/filepng.png
*** Keywords ***


Click Btn Next
    Wait Until Element Is Visible  //div[@style="text-align: center;"]/button[@class="btn btn-next mx-2"]  10s
    Click Button  //div[@style="text-align: center;"]/button[@class="btn btn-next mx-2"]
    ${Nowurl}=   Get Location
    IF   '${CreateDoc}' == '${Nowurl}'
                          Choose File  //div[@class='filepond--root my-filepond filepond--hopper']/input  ${fileJPG}
                          Wait Until Page Contains  fileJPG.jpg  60s
                          Choose File  //div[@class='filepond--root my-filepond filepond--hopper']/input  ${filepdf}
                          Wait Until Page Contains  filepdf.pdf  60s
                          Choose File  //div[@class='filepond--root my-filepond filepond--hopper']/input  ${filepng}
                          Wait Until Page Contains  filepng.png  60s
                          Wait Until Page Does Not Contain  Attachment Please check your file.  10s
                          Wait Until Element Is Visible  //div[@class='text-center p-3']/button[4]  10s
                          Click Button  //div[@class='text-center p-3']/button[4]
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT20-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(23,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser
     ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT20-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(23,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser
     END

  





*** Test Cases ***
CreateDocument Upload file-Pass
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP 
    Click Btn Create Document 
    Choose For
    Input Subject 
    Choose LOA 
    Choose Approval 1
    Choose Approval 2
    Click Btn Next
    
    