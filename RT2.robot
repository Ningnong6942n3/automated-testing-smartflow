*** Settings ***
Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
*** Variables ***
${excel}   SmartFlow.xlsx
*** Keywords ***

Check Error Massage
    ${Error Massage}  Get Text  //div[@class='errors-wrapper']/h6
    IF    '${Error Massage}' == '* Password is required!'
              Page Should Contain   * Password is required!
              Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT2-Pass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(5,13,'PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
          ELSE
              Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT2-Notpass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(5,13,'NOT PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
    END
*** Test Cases ***
Test Login Input Username Only
    Open Website SMARTFLOW
    Input Username Only And Click Btn Login (Requestor)
    Check Error Massage
   
   

   