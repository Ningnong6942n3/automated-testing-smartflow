*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

*** Variables ***

${excel}   SmartFlow.xlsx

*** Keywords ***

Check Error Massage 
    ${ErrorMessage2}  Get Text  //div[@class="invalid-feedback"]/div[text()="Please type subject"]
    ${ErrorMessage3}  Get Text  //div[@class="invalid-feedback"]/div[text()="Please Select To"]
    Sleep  5
         IF    '${ErrorMessage2}' == 'Please type subject'
              IF    '${ErrorMessage3}' == 'Please Select To'
                      Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT12-Pass.png
                      ${wb}      Load Workbook     ${CURDIR}/${excel}
                      Log to Console   ${wb}
                      ${ws}      Set Variable  ${wb['Sheet1']}
                      Log To Console   ${ws}
                      Evaluate   $ws.cell(15,13,'PASS')
                      Evaluate   $wb.save('${excel}')
                      Close All Browsers
                 ELSE
                      Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT12-Notpass.png
                      ${wb}      Load Workbook     ${CURDIR}/${excel}
                      Log to Console   ${wb}
                      ${ws}      Set Variable  ${wb['Sheet1']}
                      Log To Console   ${ws}
                      Evaluate   $ws.cell(15,13,'NOT PASS')
                      Evaluate   $wb.save('${excel}')
                      Close All Browsers
                 END
         ELSE
              Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT12-Notpass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(15,13,'NOT PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
        END
    
    

*** Test Cases ***
Document Details-False2
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Click Btn Create Document 
    Click Btn Next
    Check Error Massage 
    
   

