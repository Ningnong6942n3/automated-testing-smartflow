*** Variables *** 
${LOA_NAME}  ทดสอบการลบ(LOA) 
${BULocation01}  https://shl-dev.brainergy.digital/work-space/loa/designer-users/edit-users/32
${MainLOA_Designer_Users}  https://shl-dev.brainergy.digital/work-space/loa/designer-users
*** Keywords ***


Click Nav LOA - Create LOA
    wait until location is  https://shl-dev.brainergy.digital/work-space/documents/waiting-for-my-approval/tab/waiting-for-my-approval  10s
    Wait Until Element Is Visible  //ul[@class='nav']/li[4]  10s
    Click Element  //ul[@class='nav']/li[4]
    Wait Until Element Is Visible  //ul[@class='nav']/li[4]/div/ul/li[2]/a  10s
    Click Element  //ul[@class='nav']/li[4]/div/ul/li[2]/a
    Wait Until Element Is Visible  //div[@class='btn-wrapper']/button  10s
    Click Button  //div[@class='btn-wrapper']/button

Create LOA - Tnput Data
    Input Text  //div[@class='form-content-wrapper']/div/div[1]/input  ${LOA_NAME}
    Wait Until Element Is Visible  //div[@class='form-content-wrapper']/div/div[2]/ng-select/div   10s
    Sleep  5
    Click Element  //div[@class='form-content-wrapper']/div/div[2]/ng-select/div 
    Wait Until Element Is Visible  //ng-dropdown-panel/div/div/div[1]   10s
    Click Element  //ng-dropdown-panel/div/div/div[1]
    Wait Until Element Is Visible  //div[@class='form-content-wrapper']/div/div[3]/textarea  10s
    Input Text  //div[@class='form-content-wrapper']/div/div[3]/textarea  LOA_Description ${LOA_NAME}


Create LOA - Tnput Data (2)
    ${Random Numbers}  generate random string  3  [NUMBERS]
    Set Global Variable  ${Random Numbers}
    Wait Until Element Is Visible  //div[@class='form-content-wrapper']/div/div[1]/input  10s
    Input Text  //div[@class='form-content-wrapper']/div/div[1]/input  ${LOA_NAME}(${Random Numbers})
    Wait Until Element Is Visible  //div[@class='form-content-wrapper']/div/div[2]/ng-select/div  10s
     Sleep  2
    Click Element  //div[@class='form-content-wrapper']/div/div[2]/ng-select/div 
    Wait Until Element Is Visible  //ng-dropdown-panel/div/div/div[1]  10s
    Click Element  //ng-dropdown-panel/div/div/div[1]
    Wait Until Element Is Visible  //div[@class='form-content-wrapper']/div/div[3]/textarea  10s
    Input Text  //div[@class='form-content-wrapper']/div/div[3]/textarea  LOA_Description ${LOA_NAME}(${Random Numbers})
    

Choose Manage Approver (2)
    Wait Until Element Is Visible  //div[@class='form-btn-wrapper']/div/div/button  10s
    Sleep  5
    Click Button  //div[@class='form-btn-wrapper']/div/div/button
    Wait Until Element Is Visible  //div[@class='modal-body']/div/div/div/div/div[1]/button  10s
    Click Button  //div[@class='modal-body']/div/div/div/div/div[1]/button
    ${countMA} =	Get Element Count  //div[@class='modal-body']/div/div/div
    Log To Console  ${countMA}
    Wait Until Element Is Visible  //div[@class='modal-body']/div/div/div[1]/div/ng-select  10s
    Click Element  //div[@class='modal-body']/div/div/div[1]/div/ng-select
    Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div/span[text()='${Pocition_Approver1}']  10s
    Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div/span[text()='${Pocition_Approver1}']
    Wait Until Element Is Visible  //div[@class='modal-body']/div/div/div[2]/div/ng-select  10s
    Click Element  //div[@class='modal-body']/div/div/div[2]/div/ng-select
    Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div/span[text()='${Pocition_Approver2}']  10s
    Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div/span[text()='${Pocition_Approver2}']
    Wait Until Element Is Visible  //div[@class='modal-body']/div/div[2]/button[1]  10s
    Click Button  //div[@class='modal-body']/div/div[2]/button[1]
    ${countSMA} =	Get Element Count  //tbody/tr
    Log To Console  ${countSMA}
    Set Global Variable  ${countSMA}
 
Click btn Save (2)
    Click Button  //div[@class='form-action-wrapper']/button[2]
    Wait Until Page Contains  Your LOA was created and send to the approver.  10s
    

Check Create LOA (2)
    wait until location is  https://shl-dev.brainergy.digital/work-space/loa/designer-approvers  10s
    Wait Until Element Is Visible  //tbody/tr/td  40s
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button[1]  10s
    Sleep  5
    Click Button  //div[@class='col-sm-12 search-box-section']/button[1] 
    Wait Until Element Is Visible  //div[@class='card table-filter']/div/div[4]/ng-select  10s
    Click Element  //div[@class='card table-filter']/div/div[4]/ng-select
    Wait Until Element Is Visible  //ng-dropdown-panel/div/div/div[2]  10s
    Click Element  //ng-dropdown-panel/div/div/div[2]
    Wait Until Element Is Visible  //div[@class='card table-filter']/div/div[2]/input  10s
    Input Text  //div[@class='card table-filter']/div/div[2]/input  ${LOA_NAME}(${Random Numbers})
    ${count} =	Get Element Count  //tbody/tr
    Log To Console  ${count}
    Wait Until Element Is Visible  //tbody/tr/td  10s
    ${Show}  Get Text  //tbody/tr/td
    Log to Console   ${Show}
          IF    '${Show}' == 'No matching records found'
            Log to Console   No matching records found
          ELSE IF  '${Show}' == '${Show}'
            ${Name_LOAS}  Get Text  //tbody/tr[1]/td[3]
             IF    '${Name_LOAS}' == '${LOA_NAME}(${Random Numbers})'
                ${Num_LOAS}  Get Element Count  //tbody/tr[1]/td[5]/div/ul/li
               IF    ${Num_LOAS} == ${countSMA}
                   Log to Console  Yes
                ELSE 
                   Log to Console  No
               END  
             ELSE 
               Log to Console  No
             END  
          END

Choose Tab LOA (2)
    wait until location is  https://shl-dev.brainergy.digital/work-space/documents/waiting-for-my-approval/tab/waiting-for-my-approval  10s
    Click Element  //app-sidebar/nav/ul/li[2] 
    Wait Until Element Is Visible  //app-sidebar/nav/ul/li[2]/div/ul/li[2]/a  10s
    Click Element  //app-sidebar/nav/ul/li[2]/div/ul/li[2]/a
    Wait Until Element Is Visible  //ul[@class='navbar-datatable nav']/li[3]/a  10s
    Click Element  //ul[@class='navbar-datatable nav']/li[3]/a
    
Click btn Save 
    Click Button  //div[@class='form-action-wrapper']/button[3]
    Wait Until Page Contains  The LOA was saved successfully  10s
    Wait Until Element Is Visible  //div[@class='form-content-wrapper']/div/div[1]/input  10s
    ${LOACode}  Get Text  //div[@class='form-content-wrapper']/div/div[1]/input
    Log to Console  ${LOACode} 
    Wait Until Element Is Visible  //ul[@class='nav']/li[4]/div/ul/li[2]/a  10s
    Sleep  5
    Click Element  //ul[@class='nav']/li[4]/div/ul/li[2]/a

Click Nav LOA 
    Wait Until Element Is Visible  //ul[@class='nav']/li[4]  10s
    Click Element  //ul[@class='nav']/li[4]
    Wait Until Element Is Visible  //ul[@class='nav']/li[4]/div/ul/li[2]/a  10s
    Click Element  //ul[@class='nav']/li[4]/div/ul/li[2]/a
    Wait Until Element Is Visible  //tbody/tr/td  20s
    ${LOA_Name}  Get Text  //tbody/tr[1]/td[3]
    Set global variable  ${LOA_Name} 
    

Click Nav LOA - Edit
    Wait Until Element Is Visible  //tbody/tr/td  10s
    Click Element  //ul[@class='nav']/li[4]
    Wait Until Element Is Visible  //tbody/tr/td  10s
    Click Element  //ul[@class='nav']/li[4]/div/ul/li[1]/a
    Wait Until Element Is Visible  //tbody/tr/td  10s
    Click Button  //table/tbody/tr[1]/td[3]/div/button
    Wait Until Element Is Visible  //tbody/tr/td  10s
    Location Should Be  ${BULocation01}
    ${Order_Bu_BF}  Get Text  //tbody/tr[1]/td[1]
    Set Global Variable  ${Order_Bu_BF}
    ${Position_Bu_BF}  Get Text  //tbody/tr[1]/td[2]
    Set Global Variable  ${Position_Bu_BF}
    ${Employees_Bu_BF}  Get Text  //tbody/tr[1]/td[3]
    Set Global Variable  ${Employees_Bu_BF}

Edit - Add Employees
    Wait Until Element Is Visible  //div[@class='form-btn-wrapper']/button[3]  10s
    Click Button  //div[@class='form-btn-wrapper']/button[3]
    Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[1]/div/div  10s
    Click Element  //div[@class='modal-body']/form/div/div[1]/div/div
    ${PositionName}  Get Text  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[12]/span
    Set Global Variable  ${PositionName}
    Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[12]  10s
    Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[12]
    Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[2]/div/div/ng-select  10s
    Click Element  //div[@class='modal-body']/form/div/div[2]/div/div/ng-select
    Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]  10s
    Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]
    Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[2]/div[2]/div  10s
    Click Element  //div[@class='modal-body']/form/div/div[2]/div[2]/div
    Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[5]  10s
    ${EmployeesName_T}  Get Text  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[5]
    Set Global Variable  ${EmployeesName_T}
    ${EmployeesName}=    Fetch From Left    ${EmployeesName_T}  (
    Set Global Variable  ${EmployeesName}
    Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[5]  10s
    Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[5]
    Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[3]/button[1]  10s
    Click Button  //div[@class='modal-body']/form/div/div[3]/button[1]


Edit - Add Employees (Don't Save)
    Wait Until Element Is Visible  //div[@class='form-btn-wrapper']/button[3]  10s
    Click Button  //div[@class='form-btn-wrapper']/button[3]
    Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[1]/div/div  10s
    Click Element  //div[@class='modal-body']/form/div/div[1]/div/div
    
    ${PositionName}  Get Text  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[2]/span
    Log to Console  ${PositionName}
    Set Global Variable  ${PositionName}
    Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[2]  10s
    Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]
    
    Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[2]/div/div/ng-select  10s
    Click Element  //div[@class='modal-body']/form/div/div[2]/div/div/ng-select
    Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]  10s
    Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[1]

    Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[2]/div[2]/div  10s
    Click Element  //div[@class='modal-body']/form/div/div[2]/div[2]/div
    Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[6]  10s
    ${EmployeesName_T}  Get Text  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[6]
    Set Global Variable  ${EmployeesName_T}
    ${EmployeesName}=    Fetch From Left    ${EmployeesName_T}  (
    Set Global Variable  ${EmployeesName}
    Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[6]  10s
    Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div[6]

    Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[3]/button[1]  10s
    Click Button  //div[@class='modal-body']/form/div/div[3]/button[1]

Click Nav LOA - Edit (Tab)
    Wait Until Element Is Visible  //ul[@class='nav']/li[4]  10s
    Click Element  //ul[@class='nav']/li[4]
    Wait Until Element Is Visible  //ul[@class='nav']/li[4]/div/ul/li[1]/a  10s
    Click Element  //ul[@class='nav']/li[4]/div/ul/li[1]/a
    Wait Until Element Is Visible  //table/tbody/tr[1]/td[3]/div/button  10s
    Click Button  //table/tbody/tr[1]/td[3]/div/button
    wait until location is  ${BULocation01}  10s


Click Nav LOA - LOA Designer Users
    Wait Until Element Is Visible  //ul[@class='nav']/li[4]  10s
    Click Element  //ul[@class='nav']/li[4]
    Wait Until Element Is Visible  //ul[@class='nav']/li[4]/div/ul/li[1]/a  10s
    Click Element  //ul[@class='nav']/li[4]/div/ul/li[1]/a
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button  10s
    Click Button  //div[@class='col-sm-12 search-box-section']/button


Choose Tab LOA 
    Wait Until Element Is Visible  //app-sidebar/nav/ul/li[2]  10s
    Click Element  //app-sidebar/nav/ul/li[2] 
    Wait Until Element Is Visible  //app-sidebar/nav/ul/li[2]/div/ul/li[2]/a  10s
    Click Element  //app-sidebar/nav/ul/li[2]/div/ul/li[2]/a
    Wait Until Element Is Visible  //ul[@class='navbar-datatable nav']/li[3]/a  10s
    Click Element  //ul[@class='navbar-datatable nav']/li[3]/a

Click Nav LOA - Quick Approver
    Wait Until Element Is Visible  //ul[@class='nav']/li[4]  10s
    Click Element  //ul[@class='nav']/li[4]
    Wait Until Element Is Visible  //ul[@class='nav']/li[4]/div/ul/li[2]/a  10s
    Click Element  //ul[@class='nav']/li[4]/div/ul/li[2]/a
    
    