*** Settings ***
Library     BuiltIn

*** Variables *** 
${UserManuals}  https://shl-dev.brainergy.digital/work-space/user-manuals
${DC_USERManuals}  https://shl-dev.brainergy.digital/assets/usermanualfile/SmartFLOW_P1_User%20Manual_20210421_V2.02.pdf
${TimeLoad}  120s
*** Keywords ***

Click Btn Create Document 
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Click Button  //div[@class='container-btn']/div/div/div/button
    Wait Until Element Is Visible  //div[@class='container-btn']/div/div/div/div  10s
    Click Element  //div[@class='container-btn']/div/div/div/div
    Wait Until Element Is Visible  //div[@class="active tab-pane"]/div[@class="grid"]/ul/li/div[@class="card card-templates"]  10s
    Click Element  //div[@class="active tab-pane"]/div[@class="grid"]/ul/li/div[@class="card card-templates"]
    ${CreateDocumenturl}=   Get Location 
    Set Global Variable  ${CreateDocumenturl}
Choose For
    wait until location is  https://shl-dev.brainergy.digital/work-space/documents/my-document/create-memo  10s
    Wait Until Element Is Visible  //ng-select[@ng-reflect-name="for"]  20s
    Sleep  3
    Click Element  //ng-select[@ng-reflect-name="for"]  
    Wait Until Element Is Visible  //div[@class="ng-dropdown-panel-items scroll-host"]/div/div[1]  20s
    Click Element  //div[@class="ng-dropdown-panel-items scroll-host"]/div/div[1]   

Input Subject 
    ${Random Numbers}  generate random string  2  [NUMBERS]
    Input Text  //div[@class="col-sm-9"]/input  ทดสอบการสร้างเอกสาร${Random Numbers}

Choose LOA 
    Click Element  //div[@class="col-sm-6"]/ng-select[@bindlabel="fullname_th"]
    Wait Until Element Is Enabled  //div[@class="ng-dropdown-panel-items scroll-host"]  10s
    Click Element  //div[@class="ng-dropdown-panel-items scroll-host"]/div/div[9]
   
Choose Approval 1
    Click Element  //div[@class="col-sm-7"]/div[@class="row"]/div[1]/ng-select
    Wait Until Element Is Enabled  //div[@class="ng-dropdown-panel-items scroll-host"]  10s
    Click Element  //div[@class="ng-dropdown-panel-items scroll-host"]/div/div[9]
 
Choose Approval 2
    Click Element  //div[@class="col-sm-7"]/div[@class="row"]/div[2]/ng-select
    Wait Until Element Is Enabled  //div[@class="ng-dropdown-panel-items scroll-host"]  10s
    Click Element  //div[@class="ng-dropdown-panel-items scroll-host"]/div/div[10]
  
Click Btn Next
    Click Button  //div[@style="text-align: center;"]/button[@class="btn btn-next mx-2"]  

Input Article  
    Input Text  //quill-editor/div/div[@class='ql-editor ql-blank']  BRAINERGY เบรนเนอร์จี เริ่มก่อตั้งเมื่อเดือนธันวาคม 2561 โดยมุ่งหวังเพื่อช่วยให้องค์การของประเทศไทยสามารถใช้ประโยชน์จากเทคโนโลยีดิจิตอลในการทำงานและขยายโอกาสในการทำธุรกิจผ่านระบบดิจิตอลด้วย BRAINERGY เป็นบริษัทในเครือของเบญจจินดาผู้พัฒนาเทคโนโลยีทางด้านการสื่อสารไทยมามากกว่า 50 ปี ทำให้ BRAINERGY สามารถนำศักยภาพเทคโนโลยีดิจิตอลมาตอบโจทย์และเข้าใจการทำงานของธุรกิจไทยได้อย่างเหมาะสม

    