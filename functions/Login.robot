*** Variables *** 
${RequestorID}  waritsara_s
${Requestor_P}  Tuan#9293
${BROWSER}  Chrome
${OTP}  https://shl-dev.brainergy.digital/accounts/otp-confirm
${MAINWEB}  https://shl-dev.brainergy.digital/work-space/documents/my-document/tab/my-document
${URL}  https://shl-dev.brainergy.digital/accounts/login
${MAINWEB_Requester}  https://shl-dev.brainergy.digital/work-space/documents/my-document/tab/my-document
*** Keywords ***


Open Website SMARTFLOW
    Open Browser  ${URL}  ${BROWSER}
    Maximize Browser Window


Requestor Login 
    wait until location is  https://shl-dev.brainergy.digital/accounts/login  10s 
    ${CheckURL}  Run Keyword And Return Status   Wait Until Page Contains  WELCOME  20s  
    Run Keyword If  '${CheckURL}' != 'True'  Go To  ${URL}
    wait until location is  https://shl-dev.brainergy.digital/accounts/login  10s 
    Input Text  //form/div/input[@ng-reflect-name='username']  ${RequestorID}
    Wait Until Element Is Visible  //form/div/input[@ng-reflect-name='username']  10s
    Input Text  //form/div/input[@ng-reflect-name='password']  ${Requestor_P}   
    Wait Until Element Is Visible  //form/div/input[@ng-reflect-name='password']  10s

    

Approver Login (MK) 
    wait until location is  https://shl-dev.brainergy.digital/accounts/login  10s 
    ${CheckURL}  Run Keyword And Return Status   Wait Until Page Contains  WELCOME  20s  
    Run Keyword If  '${CheckURL}' != 'True'  Go To  ${URL} 
    Input Text  //div[@class='w-100 my-auto']/div/form/div/input[@formcontrolname='username']  mongkol_m
    Wait Until Element Is Visible  //form/div/input[@ng-reflect-name='username']  10s
    Input Text  //div[@class='form-group']/input[@class='input-password form-control ng-untouched ng-pristine ng-invalid']  Oley@123
    Wait Until Element Is Visible  //form/div/input[@ng-reflect-name='password']  10s

Approver Login (DR) 
    wait until location is  https://shl-dev.brainergy.digital/accounts/login  10s 
    ${CheckURL}  Run Keyword And Return Status   Wait Until Page Contains  WELCOME  20s  
    Run Keyword If  '${CheckURL}' != 'True'  Go To  ${URL}
    Input Text  //div[@class='w-100 my-auto']/div/form/div/input[@formcontrolname='username']  darin_p
    Wait Until Element Is Visible  //form/div/input[@ng-reflect-name='username']  10s
    Input Text  //div[@class='form-group']/input[@class='input-password form-control ng-untouched ng-pristine ng-invalid']  1234
    Wait Until Element Is Visible  //form/div/input[@ng-reflect-name='password']  10s

Approver Login (PS) 
    wait until location is  https://shl-dev.brainergy.digital/accounts/login  10s 
    ${CheckURL}  Run Keyword And Return Status   Wait Until Page Contains  WELCOME  20s  
    Run Keyword If  '${CheckURL}' != 'True'  Go To  ${URL}
    Input Text  //div[@class='w-100 my-auto']/div/form/div/input[@formcontrolname='username']  pakasit_w
    Wait Until Element Is Visible  //form/div/input[@ng-reflect-name='username']  10s
    Input Text  //div[@class='form-group']/input[@class='input-password form-control ng-untouched ng-pristine ng-invalid']  1234
    Wait Until Element Is Visible  //form/div/input[@ng-reflect-name='password']  10s

Requester - Auditor
    wait until location is  https://shl-dev.brainergy.digital/accounts/login  10s 
    ${CheckURL}  Run Keyword And Return Status   Wait Until Page Contains  WELCOME  20s  
    Run Keyword If  '${CheckURL}' != 'True'  Go To  ${URL} 
    Input Text  //div[@class='w-100 my-auto']/div/form/div/input[@formcontrolname='username']  auditor
    Wait Until Element Is Visible  //form/div/input[@ng-reflect-name='username']  10s
    Input Text  //div[@class='form-group']/input[@class='input-password form-control ng-untouched ng-pristine ng-invalid']  1234

Approve - Audit_director
    wait until location is  https://shl-dev.brainergy.digital/accounts/login  10s 
    ${CheckURL}  Run Keyword And Return Status   Wait Until Page Contains  WELCOME  20s  
    Run Keyword If  '${CheckURL}' != 'True'  Go To  ${URL}
    Input Text  //div[@class='w-100 my-auto']/div/form/div/input[@formcontrolname='username']  audit_director
    Wait Until Element Is Visible  //form/div/input[@ng-reflect-name='username']  10s
    Input Text  //div[@class='form-group']/input[@class='input-password form-control ng-untouched ng-pristine ng-invalid']  1234


Click Btn Login (Requestor)
    Click Button  //form/div/button 
    ${CheckButton}  Run Keyword And Return Status  Wait Until Element Is Visible  //h6[contains(text(),'* Username or password is incorrect')]
    Run Keyword If  '${CheckButton}' == 'True'  Click Button  //form/div/button 
    Wait Until Location Is  ${OTP}  20s

Click Btn Login 
    Click Button  //form/div/button 
    ${CheckButton}  Run Keyword And Return Status  Wait Until Element Is Visible  //h6[contains(text(),'* Username or password is incorrect')]
    Run Keyword If  '${CheckButton}' == 'True'  Click Button  //form/div/button 
    Wait Until Location Is  ${OTP}  20s


    
    

Input OTP And Click OTP
    wait until location is  https://shl-dev.brainergy.digital/accounts/otp-confirm  10s
    Wait Until Element Is Visible  //div[@class='otp-item']/input[@formcontrolname='digit_1']  10s
    Input Text  //div[@class='otp-item']/input[@formcontrolname='digit_1']  9
    Wait Until Element Is Visible  //div[@class='otp-item']/input[@formcontrolname='digit_2']  10s
    Input Text  //div[@class='otp-item']/input[@formcontrolname='digit_2']  9
    Wait Until Element Is Visible  //div[@class='otp-item']/input[@formcontrolname='digit_3']  10s
    Input Text  //div[@class='otp-item']/input[@formcontrolname='digit_3']  9
    Wait Until Element Is Visible  //div[@class='otp-item']/input[@formcontrolname='digit_4']  10s
    Input Text  //div[@class='otp-item']/input[@formcontrolname='digit_4']  9
    Wait Until Element Is Visible  //div[@class='otp-item']/input[@formcontrolname='digit_5']  10s
    Input Text  //div[@class='otp-item']/input[@formcontrolname='digit_5']  9
    Wait Until Element Is Visible  //div[@class='otp-item']/input[@formcontrolname='digit_6']  10s
    Input Text  //div[@class='otp-item']/input[@formcontrolname='digit_6']  9
    Click Button  //div[@class='otp-footer']/button

Input OTP And Click OTP Entered Incorrectly
    Input Text  //div[@class='otp-item']/input[@formcontrolname='digit_1']  0
    Wait Until Element Is Visible  //div[@class='otp-item']/input[@formcontrolname='digit_1']  10s
    Input Text  //div[@class='otp-item']/input[@formcontrolname='digit_2']  0
    Wait Until Element Is Visible  //div[@class='otp-item']/input[@formcontrolname='digit_2']  10s
    Input Text  //div[@class='otp-item']/input[@formcontrolname='digit_3']  0
    Wait Until Element Is Visible  //div[@class='otp-item']/input[@formcontrolname='digit_3']  10s
    Input Text  //div[@class='otp-item']/input[@formcontrolname='digit_4']  0
    Wait Until Element Is Visible  //div[@class='otp-item']/input[@formcontrolname='digit_4']  10s
    Input Text  //div[@class='otp-item']/input[@formcontrolname='digit_5']  0
    Wait Until Element Is Visible  //div[@class='otp-item']/input[@formcontrolname='digit_5']  10s
    Input Text  //div[@class='otp-item']/input[@formcontrolname='digit_6']  0
    Wait Until Element Is Visible  //div[@class='otp-item']/input[@formcontrolname='digit_6']  10s
    Click Button  //div[@class='otp-footer']/button



Input Username Only And Click Btn Login (Requestor)
    Input Text  //form/div/input[@ng-reflect-name='username']  ${RequestorID}
    Wait Until Element Is Visible  //form/div/input[@ng-reflect-name='username']  10s
    Click Button  //form/div/button

Input Password Only And Click Btn Login (Requestor)
    Input Text  //form/div/input[@ng-reflect-name='password']  ${Requestor_P} 
    Wait Until Element Is Visible  //form/div/input[@ng-reflect-name='password']  10s
    Click Button  //form/div/button

Click Btn Login Only And Not Iuput Data (Requestor)
    Click Button  //form/div/button

Requestor Login Entered Incorrectly And Click Btn Login (Requestor)
    Input Text  //form/div/input[@ng-reflect-name='username']  ${RequestorID}S
    Wait Until Element Is Visible  //form/div/input[@ng-reflect-name='username']  10s
    Input Text  //form/div/input[@ng-reflect-name='password']  ${Requestor_P}S
    Wait Until Element Is Visible  //form/div/input[@ng-reflect-name='password']  10s
    Click Button  //form/div/button 

Requestor Login Username Entered Incorrectly And Click Btn Login (Requestor)
    Input Text  //form/div/input[@ng-reflect-name='username']  ${RequestorID}S
    Wait Until Element Is Visible  //form/div/input[@ng-reflect-name='username']  10s
    Click Button  //form/div/button 

Requestor Login Password Entered Incorrectly And Click Btn Login (Requestor)
    Input Text  //form/div/input[@ng-reflect-name='password']  ${Requestor_P}S
    Wait Until Element Is Visible  //form/div/input[@ng-reflect-name='password']  10s
    Click Button  //form/div/button 


Sign Out 
    Wait Until Element Is Visible  //div[@class='navbar-menu-wrapper d-flex align-items-stretch']/ul[@class='navbar-nav navbar-nav-right']/li/a  10s
    Click Element  //div[@class='navbar-menu-wrapper d-flex align-items-stretch']/ul[@class='navbar-nav navbar-nav-right']/li/a
    Wait Until Element Is Visible  //div[@class='navbar-menu-wrapper d-flex align-items-stretch']/ul/li/a  10s
    Click Element  //div[@class='dropdown-menu navbar-dropdown show']/a
    wait until location is  ${URL}  10s  