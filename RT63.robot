*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot
Resource    functions/Create LOA.robot
Resource    functions/Quick Approve.robot

*** Variables ***
${excel}   SmartFlow.xlsx
${text}  Pass 
${NUMBER}  1
${InputText}  Return to Requester
${Return}  Returned
${Exit}
*** Keywords ***



Click Btn Return
    Click Button  //div[@class='text-center mt-1']/button[2]
    ${Return}  Get Text  //div[@class='modal-body pt-0']/div/div/h3
    Should Contain  Return  ${Return} 
    Input Text  //div[@class='modal-body pt-0']/div/div[2]/textarea  Return to Requester
    Click Button  //div[@class='modal-footer border-top-0 d-block pt-0']/div/button
    Wait Until Page Contains  Your document was return.  10s
    wait until location is  https://shl-dev.brainergy.digital/work-space/documents/waiting-for-my-approval/tab/waiting-for-my-approval  10s
    Sleep  5
    Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT63-Pass.png
    ${wb}      Load Workbook     ${CURDIR}/${excel}
    Log to Console   ${wb}
    ${ws}      Set Variable  ${wb['Sheet1']}
    Log To Console   ${ws}
    Evaluate   $ws.cell(66,13,'PASS')
    Evaluate   $wb.save('${excel}')
    Close Browser

Check Status Return
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Click Button  //div[@class='col-sm-12 search-box-section']/button[2]
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/input  10s
    Input Text  //div[@class='col-sm-12 search-box-section']/input  ${NameDocument}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    IF    '${Show}' == 'No matching records found'
        Capture Page Screenshot   E:/Smartflow robot/Screenshor_Notpass/RT63-Notpass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(66,13,'NOT PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser 
    ELSE
    ${CountShowitems} =	Get Element Count  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a
    ${countAll}  Get Text  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[${CountShowitems}]
    ${CheckShowData} =	Get Element Count  //tbody/tr
    Log To Console  ${CheckShowData}
    Sleep  2 
    ${Show}  Get Text  //tbody/tr/td
    FOR    ${z}    IN RANGE    ${countAll}
    ${Zn}  Evaluate  ${z}+1
    FOR    ${i}    IN RANGE    ${CheckShowData}
     ${n}  Evaluate  ${i}+1
     IF    ${CheckShowData} == ${NUMBER}
                                ${Show}  Get Text  //tbody/tr[1]/td[1]/button/p
                                IF  '${Show}' == '${NameDocument}'
                                    ${Show}  Get Text  //tbody/tr[1]/td[9]
                                    IF    '${Show}' == '${Return}'
                                        Sleep  5
                                        Click Button  //tbody/tr[1]/td[10]/button
                                        Sleep  5
                                        ${Status}  Get Text  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/label/label/span
                                        Should Contain     Returned     ${Status}
                                        Click Element  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/label/label/button
                                        ${MailReturn}  Get Text  //div[@class='modal-body']/div/h6
                                        Should Contain    Return Reason :    ${MailReturn}
                                        ${TextReturn}  Get Text  //div[@class='modal-body']/div/p
                                        Should Contain    ${InputText}    ${TextReturn}
                                        Sleep  5
                                        Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT63-Pass.png
                                        ${wb}      Load Workbook     ${CURDIR}/${excel}
                                        Log to Console   ${wb}
                                        ${ws}      Set Variable  ${wb['Sheet1']}
                                        Log To Console   ${ws}
                                        Evaluate   $ws.cell(66,13,'PASS')
                                        Evaluate   $wb.save('${excel}')
                                        Close Browser
                                    ELSE 
                                        Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT63-Notpass.png
                                        ${wb}      Load Workbook     ${CURDIR}/${excel}
                                        Log to Console   ${wb}
                                        ${ws}      Set Variable  ${wb['Sheet1']}
                                        Log To Console   ${ws}
                                        Evaluate   $ws.cell(66,13,'NOT PASS')
                                        Evaluate   $wb.save('${excel}') 
                                        Close Browser
                                    END
                                ELSE
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT63-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(66,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                                END
    ELSE IF    ${CheckShowData} > ${NUMBER}
                                ${Show}  Get Text  //tbody/tr[${n}]/td[1]/button/p
                                IF  '${Show}' == '${NameDocument}'
                                     ${Show}  Get Text  //tbody/tr[${n}]/td[9]
                                     IF    '${Show}' == '${Return}'
                                        Sleep  5
                                        Click Button  //tbody/tr[${n}]/td[9]/button
                                        Sleep  5
                                        ${Status}  Get Text  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/label/label/span
                                        Should Contain     Returned     ${Status}
                                        Click Element  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/label/label/button
                                        ${MailReturn}  Get Text  //div[@class='modal-body']/div/h6
                                        Should Contain    Return Reason :    ${MailReturn}
                                        ${TextReturn}  Get Text  //div[@class='modal-body']/div/p
                                        Should Contain    ${InputText}    ${TextReturn}
                                        Sleep  5
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT63-Pass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(66,13,'PASS')
                                    Evaluate   $wb.save('${excel}')
                                    ${Exit}  Set Variable  Exit
                                    Set Global Variable  ${Exit}
                                    Exit For Loop IF  "${Exit}" == "Exit"  
                                ELSE
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT63-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(66,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                            END
                        Exit For Loop IF  "${Exit}" == "Exit"  
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT63-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(66,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          
                    END
             Exit For Loop IF  "${Exit}" == "Exit"  
         END
          Exit For Loop IF  "${Exit}" == "Exit"  
    END 
        Exit For Loop IF  "${Exit}" == "Exit"  
        Run Keyword and Ignore Error   Click Element  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[text()='${Zn}']
    END 
    Close Browser
    END






*** Test Cases ***
WAITING FOR MY APPROVAL_QuickApprove-Return
    Open Website SMARTFLOW
    Approver Login (MK)
    Click Btn Login 
    Input OTP And Click OTP
    Click btn Quick Approve
    Click Btn Return
    
   
    
    
    

    
    
    