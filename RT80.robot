*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1
${PopUpCancle}  Are you sure to cancel?
${Employees_NA}
${LOANameError}  Please insert LOA Name
${BusinessUnitError}  Please select BU
${LOADescriptionError}  Please insert LOA Description
${ManageApproverError}  Please select Approver
*** Keywords ***





Check Error Messages 
    wait until location is  https://shl-dev.brainergy.digital/work-space/loa/designer-approvers/create-loa  10s
    Wait Until Element Is Visible  //div[@class='form-action-wrapper']/button[2]  10s
    Click Button  //div[@class='form-action-wrapper']/button[2]
    Wait Until Element Is Visible  //div[@class='form-content-wrapper']/div/div[1]/div/div  10s
    ${LOAName_Error}  Get Text  //div[@class='form-content-wrapper']/div/div[1]/div/div
    Wait Until Element Is Visible  //div[@class='form-content-wrapper']/div/div[2]/div/div  10s
    ${BusinessUnit_Error}  Get Text  //div[@class='form-content-wrapper']/div/div[2]/div/div
    Wait Until Element Is Visible  //div[@class='form-content-wrapper']/div/div[3]/div/div  10s
    ${LOADescription_Error}  Get Text  //div[@class='form-content-wrapper']/div/div[3]/div/div
    Wait Until Element Is Visible  //div[@class='form-btn-wrapper']/div/div/div/div  10s
    ${ManageApprover_Error}  Get Text  //div[@class='form-btn-wrapper']/div/div/div/div
    IF   '${LOAName_Error}' == '${LOANameError}'
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT80-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(83,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                        
                              IF   '${BusinessUnit_Error}' == '${BusinessUnitError}'
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT80-Pass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(83,13,'PASS')
                                    Evaluate   $wb.save('${excel}')

                                            IF   '${LOADescription_Error}' == '${LOADescriptionError}'
                                                Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT80-Pass.png
                                                ${wb}      Load Workbook     ${CURDIR}/${excel}
                                                Log to Console   ${wb}
                                                ${ws}      Set Variable  ${wb['Sheet1']}
                                                Log To Console   ${ws}
                                                Evaluate   $ws.cell(83,13,'PASS')
                                                Evaluate   $wb.save('${excel}')

                                                        IF   '${ManageApprover_Error}' == '${ManageApproverError}'
                                                                Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT80-Pass.png
                                                                ${wb}      Load Workbook     ${CURDIR}/${excel}
                                                                Log to Console   ${wb}
                                                                ${ws}      Set Variable  ${wb['Sheet1']}
                                                                Log To Console   ${ws}
                                                                Evaluate   $ws.cell(83,13,'PASS')
                                                                Evaluate   $wb.save('${excel}')
                                                                Close Browser
                                                        ELSE
                                                                Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT80-Notpass.png
                                                                ${wb}      Load Workbook     ${CURDIR}/${excel}
                                                                Log to Console   ${wb}
                                                                ${ws}      Set Variable  ${wb['Sheet1']}
                                                                Log To Console   ${ws}
                                                                Evaluate   $ws.cell(83,13,'NOT PASS')
                                                                Evaluate   $wb.save('${excel}')
                                                                Close Browser
                                                        END
                                            ELSE
                                                Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT80-Notpass.png
                                                ${wb}      Load Workbook     ${CURDIR}/${excel}
                                                Log to Console   ${wb}
                                                ${ws}      Set Variable  ${wb['Sheet1']}
                                                Log To Console   ${ws}
                                                Evaluate   $ws.cell(83,13,'NOT PASS')
                                                Evaluate   $wb.save('${excel}')
                                                Close Browser
                                            END
                              ELSE
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT80-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(83,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                                    Close Browser
                              END
    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT80-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(83,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser
    END

*** Test Cases ***
Designer Approvers - CreateLOA_Error01
    Open Website SMARTFLOW
    Requester - Auditor
    Click Btn Login 
    Input OTP And Click OTP
    Click Nav LOA - Create LOA
    Check Error Messages 
    
    
    
    
  
    
    
   
    
    

    
    
    