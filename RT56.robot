*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

*** Variables ***
${excel}   SmartFlow.xlsx 
${NUMBER}  1
${TimeCreateApprove}
${Exit}
${Document}  ทดสอบการอนุมัติเอกสาร
*** Keywords ***
Input Subject (Random Numbers)
    ${Random Numbers}  generate random string  4  [NUMBERS]
    Set Global Variable  ${Random Numbers}
    Input Text  //div[@class="col-sm-9"]/input  ${Document}(${Random Numbers})

Input Article
    Page Should Not Contain  Delete
    Input Text  //quill-editor/div/div[@class='ql-editor ql-blank']  BRAINERGY เบรนเนอร์จี เริ่มก่อตั้งเมื่อเดือนธันวาคม 2561 โดยมุ่งหวังเพื่อช่วยให้องค์การของประเทศไทยสามารถใช้ประโยชน์จากเทคโนโลยีดิจิตอลในการทำงานและขยายโอกาสในการทำธุรกิจผ่านระบบดิจิตอลด้วย BRAINERGY เป็นบริษัทในเครือของเบญจจินดาผู้พัฒนาเทคโนโลยีทางด้านการสื่อสารไทยมามากกว่า 50 ปี ทำให้ BRAINERGY สามารถนำศักยภาพเทคโนโลยีดิจิตอลมาตอบโจทย์และเข้าใจการทำงานของธุรกิจไทยได้อย่างเหมาะสม
Click btn Send
    Click Button  //div[@class='text-center p-3']/button[4]
    Wait Until Page Contains  Your document was created and sent to the approver.  10s
    Wait Until Location Is  ${MAINWEB}  10s

Click Btn Preview - Approve
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Wait Until Element Is Visible  //tbody/tr[1]/td/button/p  10s
    Sleep  5
    Click Button  //div[@class='col-sm-12 search-box-section']/button[2]
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/input
    input Text  //div[@class='col-sm-12 search-box-section']/input  ${Document}(${Random Numbers})
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${StatusData}  Get Text  //tbody/tr[1]/td[6]/div/button 
    ${StatusDataNow}  Fetch From Right  ${StatusData}  /
    Click Button  //tbody/tr[1]/td[9]/button
    wait until location is  https://shl-dev.brainergy.digital/work-space/documents/details/BNG2021%252F${StatusDataNow}  10s
    Wait Until Element Is Visible  //div[@class='text-center']/button[4]   10s
    Sleep  5
    Click Button  //div[@class='text-center']/button[4]   
    Wait Until Page Contains  Your document was approved and sent to the next level.  10s
    ${Time}  Get Current Date  result_format=%d/%m/%Y %I:%M %p
    ${TimeCreateApprove}=  Convert To String  ${Time}
    Log To Console  ${TimeCreateApprove}
    Wait until location is  https://shl-dev.brainergy.digital/work-space/documents/waiting-for-my-approval/tab/waiting-for-my-approval  10s



Check Status On Process
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Click Button  //div[@class='col-sm-12 search-box-section']/button[2]
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button[2]  10s
    Input Text  //div[@class='col-sm-12 search-box-section']/input  ${Document}(${Random Numbers})
    ${CheckShowData} =	Get Element Count  //tbody/tr
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    IF    '${Show}' == 'No matching records found'
        Capture Page Screenshot  E:/TEstAll/Screenshot_Pass/RT55-Pass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(58,13,'PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser 
    ELSE
    ${CountShowitems} =	Get Element Count  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a
    ${countAll}  Get Text  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[${CountShowitems}]
    ${CheckShowData} =	Get Element Count  //tbody/tr
    Log To Console  ${CheckShowData}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    FOR    ${z}    IN RANGE    ${countAll}
    ${Zn}  Evaluate  ${z}+1
    FOR    ${i}    IN RANGE    ${CheckShowData}
     ${n}  Evaluate  ${i}+1
     IF    ${CheckShowData} == ${NUMBER}
        ${Show}  Get Text  //tbody/tr[1]/td[9]/button
                    IF   '${Show}' == 'On Process'
                          Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
                          ${StatusData}  Get Text  //tbody/tr[1]/td[7]/div/button 
                          ${StatusDataNow}  Fetch From Right  ${StatusData}  /
                          Click Button  //tbody/tr[1]/td[10]/button
                          wait until location is  https://shl-dev.brainergy.digital/work-space/documents/details/BNG2021%252F${StatusDataNow}  10s
                          Wait Until Element Is Visible  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/p/label/span[1]  10s
                          ${Status}  Get Text  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/p/label/span[1]
                          Should Contain       On Process       ${Status}
                          Click Element  //div[@class='right-wrapper d-sm-none d-md-block d-none']/div/property-nav/div/ul[@class='nav-pills nav-vertical nav flex-column']/li[3]
                          ${countht} =	Get Element Count  //div[@class='right-wrapper d-sm-none d-md-block d-none']/div/property-nav/div/div/div/property-audit-trail/div/div[2]/ul/li
                          Log To Console  ${countht}
                          Wait Until Element Is Visible  //div[@class='right-wrapper d-sm-none d-md-block d-none']/div/property-nav/div/ul[@class='nav-pills nav-vertical nav flex-column']/li[3]  10s
                          Page Should Contain  Waritsara Sonchan  
                          Page Should Contain  Create this document  
                          Page Should Contain  Mongkol Mulyongsri  
                          Page Should Contain  Approve this document  
                          Page Should Contain  ${TimeCreateApprove}
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT56-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(59,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser 
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT56-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(59,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser 
                    END
    ELSE IF    ${CheckShowData} > ${NUMBER}
                    ${Show}  Get Text  //tbody/tr[${n}]/td[9]/button
                    IF   '${Show}' == 'On Process'
                          Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
                          ${StatusData}  Get Text  //tbody/tr[${n}]/td[7]/div/button 
                          ${StatusDataNow}  Fetch From Right  ${StatusData}  /
                          Click Button  //tbody/tr[${n}]/td[10]/button
                          wait until location is  https://shl-dev.brainergy.digital/work-space/documents/details/BNG2021%252F${StatusDataNow}  10s
                          Wait Until Element Is Visible  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/p/label/span[1]  10s
                          ${Status}  Get Text  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/p/label/span[1]
                          Should Contain       On Process       ${Status}
                          Click Element  //div[@class='right-wrapper d-sm-none d-md-block d-none']/div/property-nav/div/ul[@class='nav-pills nav-vertical nav flex-column']/li[3]
                          ${countht} =	Get Element Count  //div[@class='right-wrapper d-sm-none d-md-block d-none']/div/property-nav/div/div/div/property-audit-trail/div/div[2]/ul/li
                          Log To Console  ${countht}
                          Wait Until Element Is Visible  //div[@class='right-wrapper d-sm-none d-md-block d-none']/div/property-nav/div/ul[@class='nav-pills nav-vertical nav flex-column']/li[3]  10s
                          Page Should Contain  Waritsara Sonchan  
                          Page Should Contain  Create this document  
                          Page Should Contain  Mongkol Mulyongsri  
                          Page Should Contain  Approve this document  
                          Page Should Contain  ${TimeCreateApprove}
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT56-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(59,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                          ${Exit}  Set Variable  Exit
                          Set Global Variable  ${Exit}
                          Exit For Loop IF  "${Exit}" == "Exit" 
                    ELSE
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(59,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          
                    END
         Exit For Loop IF  "${Exit}" == "Exit"  
         END
         Exit For Loop IF  "${Exit}" == "Exit"  
    END 
        Exit For Loop IF  "${Exit}" == "Exit"  
        Run Keyword and Ignore Error   Click Element  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[text()='${Zn}']
    END 
    Close Browser
    END

    

*** Test Cases ***
WAITING FOR MY APPROVAL Preview - Apporve
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Click Btn Create Document 
    Choose For
    Input Subject (Random Numbers)
    Choose LOA 
    Choose Approval 1
    Choose Approval 2
    Click Btn Next
    Input Article
    Click btn Send
    Sign Out
    Approver Login (MK) 
    Click Btn Login 
    Input OTP And Click OTP
    Click Btn Preview - Approve
    Sign Out 
    Requestor Login 
    Click Btn Login 
    Input OTP And Click OTP
    Check Status On Process
    
    
    
    

    
    
    