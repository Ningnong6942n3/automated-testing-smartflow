*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1
${Exit}

*** Keywords ***



Click btn Filter
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button[1]  10s
    Sleep  5
    Click Button  //div[@class='col-sm-12 search-box-section']/button[1]
    Wait Until Element Is Visible  //div[@class='card table-filter']/div/div[4]/ng-select  10s
    Click Element  //div[@class='card table-filter']/div/div[4]/ng-select
    ${Status}  Get Text  //ng-select/ng-dropdown-panel/div/div/div[6]/span
    Wait Until Element Is Visible  //ng-select/ng-dropdown-panel/div/div/div[6]  10s
    Click Element  //ng-select/ng-dropdown-panel/div/div/div[6]
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    IF    '${Show}' == 'No matching records found'
        Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT102-Pass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(105,13,'PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser 
    ELSE
    ${CountShowitems} =	Get Element Count  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a
    ${countAll}  Get Text  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[${CountShowitems}]
    ${CheckShowData} =	Get Element Count  //tbody/tr
    Log To Console  ${CheckShowData}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    FOR    ${z}    IN RANGE    ${countAll}
    ${Zn}  Evaluate  ${z}+1
    FOR    ${i}    IN RANGE    ${CheckShowData}
     ${n}  Evaluate  ${i}+1
     IF    ${CheckShowData} == ${NUMBER}
       ${Show}  Get Text  //tbody/tr[1]/td[6]/span    
                    IF   '${Show}' == '${Status}'
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT102-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(105,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT102-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(105,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          ${Exit}  Set Variable  Exit
                          Set Global Variable  ${Exit}
                          Exit For Loop IF  "${Exit}" == "Exit"  
                    END
    ELSE IF    ${CheckShowData} > ${NUMBER}
                   ${Show}  Get Text  //tbody/tr[${n}]/td[6]/span
                    IF    '${Show}' == '${Status}'
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT102-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(105,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT102-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(105,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          ${Exit}  Set Variable  Exit
                          Set Global Variable  ${Exit}
                          Exit For Loop IF  "${Exit}" == "Exit"  
                    END
         Exit For Loop IF  "${Exit}" == "Exit"  
         END
         Exit For Loop IF  "${Exit}" == "Exit"  
    END 
        Exit For Loop IF  "${Exit}" == "Exit"  
        Run Keyword and Ignore Error   Click Element  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[text()='${Zn}']
    END 
    Close Browser
    END



*** Test Cases ***
Designer Approvers - Filter_Status06
    Open Website SMARTFLOW
    Requester - Auditor
    Click Btn Login 
    Input OTP And Click OTP
    Click Nav LOA 
    Click btn Filter
 
  
    
  
    
    
   
    
    

    
    
    