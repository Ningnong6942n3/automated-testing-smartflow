*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot
Resource    functions/Quick Approve.robot
Resource    functions/Create LOA.robot

*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1
${CreateDoc}  https://shl-dev.brainergy.digital/work-space/documents/my-document/create/content
${Document}  ทดสอบการสร้างเอกสาร
${Exit}
${fileJPG}  E:/Smartflow robot/file/fileJPG.jpg
${filepdf}  E:/Smartflow robot/file/filepdf.pdf
${filepng}  E:/Smartflow robot/file/filepng.png
*** Keywords ***

Input Subject {Random Numbers}
    ${Random Numbers}  generate random string  2  [NUMBERS]
    Set Global Variable  ${Random Numbers}
    Input Text  //div[@class="col-sm-9"]/input  ${Document}(Uploadfile${Random Numbers})

Click Btn Next 
    Wait Until Element Is Visible  //div[@style="text-align: center;"]/button[@class="btn btn-next mx-2"]  10s
    Click Button  //div[@style="text-align: center;"]/button[@class="btn btn-next mx-2"]
    wait until location is  ${CreateDoc}  10s

Upload file 
    Wait Until Element Is Visible  //div[@class='filepond--wrapper']/div/div[@class='filepond--drop-label']  10s
    Choose File  //div[@class='filepond--root my-filepond filepond--hopper']/input  ${fileJPG}
    Wait Until Page Contains  fileJPG.jpg  60s
    Choose File  //div[@class='filepond--root my-filepond filepond--hopper']/input  ${filepdf}
    Wait Until Page Contains  filepdf.pdf  60s
    Choose File  //div[@class='filepond--root my-filepond filepond--hopper']/input  ${filepng}
    Wait Until Page Contains  filepng.png  60s
    Wait Until Page Does Not Contain  Attachment Please check your file.  10s
    Wait Until Element Is Visible  //div[@class='text-center p-3']/button[4]  10s

Click btn Send 
    Wait Until Element Is Visible  //div[@class='text-center p-3']/button[4]  10s
    Sleep  5
    Click Button  //div[@class='text-center p-3']/button[4]
    Wait Until Page Contains  Your document was created and sent to the approver.  10s
    wait until location is  ${MAINWEB_Requester}  10s

Search Document
    Wait Until Element Is Visible  //tbody/tr/td  60s
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button[2]  10s
    Sleep  5
    Click Button  //div[@class='col-sm-12 search-box-section']/button[2]
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/input  10s
    input Text  //div[@class='col-sm-12 search-box-section']/input  ${Document}(Uploadfile${Random Numbers})
    Wait Until Element Is Visible  //tbody/tr/td  30s
    ${Show}  Get Text  //tbody/tr/td
    IF    '${Show}' == 'No matching records found'
        Capture Page Screenshot   E:/Smartflow robot/Screenshor_Notpass/RT60-Notpass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(63,13,'NOT PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser 
    ELSE
    ${CountShowitems} =	Get Element Count  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a
    ${countAll}  Get Text  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[${CountShowitems}]
    ${CheckShowData} =	Get Element Count  //tbody/tr
    Log To Console  ${CheckShowData}
    Wait Until Element Is Visible  //tbody/tr/td  30s
    ${Show}  Get Text  //tbody/tr/td
    FOR    ${z}    IN RANGE    ${countAll}
    ${Zn}  Evaluate  ${z}+1
    FOR    ${i}    IN RANGE    ${CheckShowData}
     ${n}  Evaluate  ${i}+1
     IF    ${CheckShowData} == ${NUMBER}
       ${Show}  Get Text  //tbody/tr[1]/td[1]/button/p
                                IF  '${Show}' == '${Document}(Uploadfile${Random Numbers})'
                                     ${Show}  Get Text  //tbody/tr[1]/td[8]/button/span
                                     IF  '${Show}' == 'Waiting for my Approval'
                                        Click Button  //tbody/tr[1]/td[9]/button
                                        Wait Until Element Is Visible  //div[@class='right-container']/property-nav/div[@class='h-100 d-sm-none d-md-flex d-none nav-property']/ul/li[2]/a  10s
                                        Click Element  //div[@class='right-container']/property-nav/div[@class='h-100 d-sm-none d-md-flex d-none nav-property']/ul/li[2]/a
                                        Wait Until Page Contains  fileJPG.jpg  60s
                                        Wait Until Page Contains  filepdf.pdf  60s
                                        Wait Until Page Contains  filepng.png  60s
                                        Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT60-Pass.png
                                        ${wb}      Load Workbook     ${CURDIR}/${excel}
                                        Log to Console   ${wb}
                                        ${ws}      Set Variable  ${wb['Sheet1']}
                                        Log To Console   ${ws}
                                        Evaluate   $ws.cell(63,13,'PASS')
                                        Evaluate   $wb.save('${excel}')
                                     ELSE
                                     Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT60-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(63,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                                     END
                                ELSE
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT60-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(63,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                                END
                    
    ELSE IF    ${CheckShowData} > ${NUMBER}
                   ${Show}  Get Text  //tbody/tr[${n}]/td[1]/button/p
                                IF  '${Show}' == '${Document}(Uploadfile${Random Numbers})'
                                     ${Show}  Get Text  //tbody/tr[${n}]/td[8]/button/span
                                     IF  '${Show}' == 'Waiting for my Approval'
                                    Click Button  //tbody/tr[${n}]/td[9]/button
                                    Sleep  5
                                    Click Element  //div[@class='right-container']/property-nav/div[@class='h-100 d-sm-none d-md-flex d-none nav-property']/ul/li[2]/a
                                    Sleep  5
                                    Page Should Contain  fileJPG.jpg
                                    Page Should Contain  filepdf.pdf
                                    Page Should Contain  filepng.png
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT60-Pass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(63,13,'PASS')
                                    Evaluate   $wb.save('${excel}')
                                    ${Exit}  Set Variable  Exit
                                    Set Global Variable  ${Exit}
                                    Exit For Loop IF  "${Exit}" == "Exit"  
                                ELSE
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT60-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(63,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                            END
                        Exit For Loop IF  "${Exit}" == "Exit"  
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT60-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(63,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          
                    END
             Exit For Loop IF  "${Exit}" == "Exit"  
         END
          Exit For Loop IF  "${Exit}" == "Exit"  
    END 
        Exit For Loop IF  "${Exit}" == "Exit"  
        Run Keyword and Ignore Error   Click Element  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[text()='${Zn}']
    END 
    Close Browser
    END

    



*** Test Cases ***
Document_Uploadfile
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Click Btn Create Document 
    Choose For
    Input Subject {Random Numbers}
    Choose LOA 
    Choose Approval 1
    Choose Approval 2
    Click Btn Next
    Upload file 
    Click btn Send
    Sign Out 
    Approver Login (DR)
    Click Btn Login 
    Input OTP And Click OTP
    Search Document
    
   

    
    

    
    
    