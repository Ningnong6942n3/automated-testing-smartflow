*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot
Resource    functions/Quick Approve.robot
Resource    functions/Create LOA.robot

*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1
${WEBEdit}  https://shl-dev.brainergy.digital/work-space/profile
${fileJPG}  E:/Smartflow robot/file/fileJPG.jpg
*** Keywords ***



Click Tab Profile
    Wait Until Element Is Visible  //li[@class='nav-item nav-profile']/a  10s
    Click Element  //li[@class='nav-item nav-profile']/a

Click Btn Signature
    Wait Until Element Is Visible  //div[@class='content-wrapper']/div/app-profile/div/div/div[9]/div/div/div/button  10s
    Click Element  //div[@class='content-wrapper']/div/app-profile/div/div/div[9]/div/div/div/button
    ${SIGN_NOW}  Get Text  //div[@class='modal-body']/div/button[1]
    ${IMPORT}  Get Text  //div[@class='modal-body']/div/button[2]
    ${CANCEL}  Get Text  //div[@class='modal-body']/div/button[3]
    Page Should Contain Button  CANCEL
    Page Should Contain Button  SIGN NOW
    Page Should Contain Button  IMPORT
    IF   '${SIGN_NOW}' == 'SIGN NOW'
            IF   '${IMPORT}' == 'IMPORT'        
                    IF   '${CANCEL}' == 'CANCEL'
                            Wait Until Element Is Visible  //div[@class='modal-body']/div/button[2]  10s
                            Click Button  //div[@class='modal-body']/div/button[2]
                            Wait Until Element Is Visible  //div[@class='modal-body']/div/button  10s
                            Choose File  //div[@class='modal-body']/div/input  ${fileJPG}  
                            Sleep  5
                            Click Element  //div[@class='text-center pt-1']/button[1]
                            Wait Until Page Contains  Your profile information updated successfully.  60s
                            Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT123-Pass.png
                            ${wb}      Load Workbook     ${CURDIR}/${excel}
                            Log to Console   ${wb}
                            ${ws}      Set Variable  ${wb['Sheet1']}
                            Log To Console   ${ws}
                            Evaluate   $ws.cell(126,13,'PASS')
                            Evaluate   $wb.save('${excel}')
                            Close Browser 
                    ELSE
                            Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT123-Notpass.png
                            ${wb}      Load Workbook     ${CURDIR}/${excel}
                            Log to Console   ${wb}
                            ${ws}      Set Variable  ${wb['Sheet1']}
                            Log To Console   ${ws}
                            Evaluate   $ws.cell(126,13,'NOT PASS')
                            Evaluate   $wb.save('${excel}')
                            Close Browser 
                    END
                    
            ELSE
                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT123-Notpass.png
                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                    Log to Console   ${wb}
                    ${ws}      Set Variable  ${wb['Sheet1']}
                    Log To Console   ${ws}
                    Evaluate   $ws.cell(126,13,'NOT PASS')
                    Evaluate   $wb.save('${excel}')
                    Close Browser 
            END
    ELSE
          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT123-Notpass.png
          ${wb}      Load Workbook     ${CURDIR}/${excel}
          Log to Console   ${wb}
          ${ws}      Set Variable  ${wb['Sheet1']}
          Log To Console   ${ws}
          Evaluate   $ws.cell(126,13,'NOT PASS')
          Evaluate   $wb.save('${excel}')
          Close Browser 
    END


    
    
    
*** Test Cases ***
Document Details-Pass.
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Click Tab Profile
    Click Btn Signature
   
    
    
    
    