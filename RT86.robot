*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1
${PopUpCancle}  Are you sure to cancel?
${Employees_NA}
${Pocition_Approver1}  Department Director
${Pocition_Approver2}  Operation Manager
*** Keywords ***


    

Create LOA - Tnput Data
    Wait Until Element Is Visible  //div[@class='form-content-wrapper']/div/div[1]/input  10s
    Input Text  //div[@class='form-content-wrapper']/div/div[1]/input  LOAName01
    Wait Until Element Is Visible  //div[@class='form-content-wrapper']/div/div[2]/ng-select/div  10s
    Click Element  //div[@class='form-content-wrapper']/div/div[2]/ng-select/div 
    Wait Until Element Is Visible  //ng-dropdown-panel/div/div/div[1]  10s
    Click Element  //ng-dropdown-panel/div/div/div[1]
    Wait Until Element Is Visible  //div[@class='form-content-wrapper']/div/div[3]/textarea  10s
    Input Text  //div[@class='form-content-wrapper']/div/div[3]/textarea  LOA_Description
    

Choose Manage Approver
    Wait Until Element Is Visible  //div[@class='form-btn-wrapper']/div/div/button  10s
    Click Button  //div[@class='form-btn-wrapper']/div/div/button
    ${CheckFilter}  Run Keyword And Return Status   Wait Until Element Is Visible  //div[@class='modal-body']/div/div/div/div/div[1]/button  10s
    Run Keyword If  '${CheckFilter}' != 'True'  Click Button  //div[@class='form-btn-wrapper']/div/div/button
    Click Button  //div[@class='modal-body']/div/div/div/div/div[1]/button
    Wait Until Element Is Visible  //div[@class='modal-body']/div/div/div[1]/div/ng-select  10s
    Click Element  //div[@class='modal-body']/div/div/div[1]/div/ng-select
    Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div/span[text()='${Pocition_Approver1}']  10s
    Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div/span[text()='${Pocition_Approver1}']
    Wait Until Element Is Visible  //div[@class='modal-body']/div/div/div[2]/div/ng-select  10s
    Click Element  //div[@class='modal-body']/div/div/div[2]/div/ng-select
    Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div/span[text()='${Pocition_Approver2}']  10s
    Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div/span[text()='${Pocition_Approver2}']
    Wait Until Element Is Visible  //div[@class='modal-body']/div/div[2]/button[1]  10s
    Click Button  //div[@class='modal-body']/div/div[2]/button[1]


Search - Data Manage Approver
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button[1]  10s
    Sleep  2
    Click Button  //div[@class='col-sm-12 search-box-section']/button[1]
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Name_Approver}  Get Text  //tbody/tr[1]/td[3]
    Wait Until Element Is Visible  //div[@class='card table-filter']/div/div[2]/input  10s
    Input Text  //div[@class='card table-filter']/div/div[2]/input  ${Name_Approver}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Name_ApproverCH}  Get Text  //tbody/tr[1]/td[3]   
    ${countSMA} =	Get Element Count  //tbody/tr
    Log To Console  ${countSMA}
    IF   '${countSMA}' == '1'
                        IF   '${Name_ApproverCH}' == '${Name_Approver}'
                            Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT86-Pass.png
                            ${wb}      Load Workbook     ${CURDIR}/${excel}
                            Log to Console   ${wb}
                            ${ws}      Set Variable  ${wb['Sheet1']}
                            Log To Console   ${ws}
                            Evaluate   $ws.cell(89,13,'PASS')
                            Evaluate   $wb.save('${excel}')
                            Close Browser
                        ELSE
                            Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT86-Notpass.png
                            ${wb}      Load Workbook     ${CURDIR}/${excel}
                            Log to Console   ${wb}
                            ${ws}      Set Variable  ${wb['Sheet1']}
                            Log To Console   ${ws}
                            Evaluate   $ws.cell(89,13,'PASS')
                            Evaluate   $wb.save('${excel}')
                            Close Browser                                                      
                        END   
    ELSE                                                    
          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT86-Notpass.png
          ${wb}      Load Workbook     ${CURDIR}/${excel}
          Log to Console   ${wb}
          ${ws}      Set Variable  ${wb['Sheet1']}
          Log To Console   ${ws}
          Evaluate   $ws.cell(89,13,'PASS')
          Evaluate   $wb.save('${excel}')
          Close Browser                                                      
    END    

    


*** Test Cases ***
Designer Approvers - CreateLOA_Filter02
    Open Website SMARTFLOW
    Requester - Auditor
    Click Btn Login 
    Input OTP And Click OTP 
    Click Nav LOA - Create LOA
    Create LOA - Tnput Data
    Choose Manage Approver
    Search - Data Manage Approver
  
    
    
    
  
    
    
   
    
    

    
    
    