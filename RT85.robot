*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1
${PopUpCancle}  Are you sure to cancel?
${Employees_NA}
${Pocition_Approver}  Department Director
${Pocition_Approver2}  Operation Manager
${ManageApproverError}  Please select Approver
*** Keywords ***



Create LOA - Tnput Data
    Wait Until Element Is Visible  //div[@class='form-content-wrapper']/div/div[1]/input  10s
    Input Text  //div[@class='form-content-wrapper']/div/div[1]/input  LOAName01
    Wait Until Element Is Visible  //div[@class='form-content-wrapper']/div/div[2]/ng-select/div  10s
    Click Element  //div[@class='form-content-wrapper']/div/div[2]/ng-select/div 
    Wait Until Element Is Visible  //ng-dropdown-panel/div/div/div[1]  10s
    Click Element  //ng-dropdown-panel/div/div/div[1]
    Wait Until Element Is Visible  //div[@class='form-content-wrapper']/div/div[3]/textarea  10s
    Input Text  //div[@class='form-content-wrapper']/div/div[3]/textarea  LOA_Description
    

Choose Manage Approver
    Click Button  //div[@class='form-btn-wrapper']/div/div/button
    Wait Until Element Is Visible  //div[@class='modal-body']/div/div/div/div/div[1]/button  10s
    Click Button  //div[@class='modal-body']/div/div/div/div/div[1]/button
     ${CheckFilter}  Run Keyword And Return Status    Wait Until Element Is Visible  //div[@class='modal-body']/div/div/div[2]/div/ng-select  10s
    Run Keyword If  '${CheckFilter}' != 'True'  Click Button  //div[@class='modal-body']/div/div/div/div/div[1]/button
    Click Element  //div[@class='modal-body']/div/div/div[2]/div/ng-select
    Wait Until Element Is Visible  //div[@class='ng-dropdown-panel-items scroll-host']/div/div/span[text()='${Pocition_Approver2}']  10s
    Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div/span[text()='${Pocition_Approver2}']
    Wait Until Element Is Visible  //div[@class='modal-body']/div/div[2]/button[1]  10s
    Click Button  //div[@class='modal-body']/div/div[2]/button[1]
    Wait Until Element Is Visible  //div[@class='modal-body']/div/div[1]/div/div/div[2]/div  10s
    ${ManageApprover_Error}  Get Text  //div[@class='modal-body']/div/div[1]/div/div/div[2]/div
    IF   '${ManageApprover_Error}' == '${ManageApproverError}'
            Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT85-Pass.png
            ${wb}      Load Workbook     ${CURDIR}/${excel}
            Log to Console   ${wb}
            ${ws}      Set Variable  ${wb['Sheet1']}
            Log To Console   ${ws}
            Evaluate   $ws.cell(88,13,'PASS')
            Evaluate   $wb.save('${excel}')
            Close Browser
    ELSE
            Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT85-Notpass.png
            ${wb}      Load Workbook     ${CURDIR}/${excel}
            Log to Console   ${wb}
            ${ws}      Set Variable  ${wb['Sheet1']}
            Log To Console   ${ws}
            Evaluate   $ws.cell(88,13,'NOT PASS')
            Evaluate   $wb.save('${excel}')
            Close Browser
    END
    
 


*** Test Cases ***
Designer Approvers - CreateLOA_MyApproveError
    Open Website SMARTFLOW
    Requester - Auditor
    Click Btn Login 
    Input OTP And Click OTP
    Click Nav LOA - Create LOA
    Create LOA - Tnput Data
    Choose Manage Approver
   
    
    
    
  
    
    
   
    
    

    
    
    