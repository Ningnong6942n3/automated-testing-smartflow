*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1
${PopUpCancle}  Are you sure to cancel?
${Employees_NA}
${Pocition_Approver1}  Department Director
${Pocition_Approver2}  Operation Manager
${LOA_NAME}  ทดสอบการสร้างและบันทึกLOA - Delate
${LOACode}
${Popup_Delate}  Delete
${Popup_Delate1}  Are you sure to delete this LOA ?
${Exit}
*** Keywords ***



Check Create LOA 
    wait until location is  https://shl-dev.brainergy.digital/work-space/loa/designer-approvers  10s
    Wait Until Element Is Visible  //tbody/tr/td  20s
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button[1]   10s
    Click Button  //div[@class='col-sm-12 search-box-section']/button[1] 
    Wait Until Element Is Visible  //div[@class='card table-filter']/div/div[4]/ng-select  10s
    Click Element  //div[@class='card table-filter']/div/div[4]/ng-select
    Wait Until Element Is Visible  //ng-dropdown-panel/div/div/div[1]  10s
    Click Element  //ng-dropdown-panel/div/div/div[1]
    Wait Until Element Is Visible  //div[@class='card table-filter']/div/div[2]/input  10s
    Input Text  //div[@class='card table-filter']/div/div[2]/input  ${LOA_NAME}(${Random Numbers})
    Wait Until Element Is Visible  //tbody/tr/td  20s
    ${Show}  Get Text  //tbody/tr/td
    IF    '${Show}' == 'No matching records found'
        Capture Page Screenshot   E:/Smartflow robot/Screenshor_Notpass/RT89-Notpass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(92,13,'NOT PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser 
    ELSE
    ${CountShowitems} =	Get Element Count  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a
    ${countAll}  Get Text  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[${CountShowitems}]
    ${CheckShowData} =	Get Element Count  //tbody/tr
    Log To Console  ${CheckShowData}
    Wait Until Element Is Visible  //tbody/tr/td  20s
    ${Show}  Get Text  //tbody/tr/td
    FOR    ${z}    IN RANGE    ${countAll}
    ${Zn}  Evaluate  ${z}+1
    FOR    ${i}    IN RANGE    ${CheckShowData}
     ${n}  Evaluate  ${i}+1
     IF    ${CheckShowData} == ${NUMBER}
       ${Name_LOAS}  Get Text  //tbody/tr[1]/td[3]
                                IF  '${Name_LOAS}' == '${LOA_NAME}(${Random Numbers})'
                                     ${Show}  Get Text  //tbody/tr[1]/td[6]/span
                                     IF  '${Show}' == 'Draft'
                                            ${Num_Delate}  Get Text  //tbody/tr[1]/td[2]
                                            Click Button  //tbody/tr[1]/td[7]/div/button[2]
                                            Wait Until Element Is Visible  //div[@class='modal-body']/div/div/h3  10s
                                            ${PopupDelate}  Get Text  //div[@class='modal-body']/div/div/h3
                                            Should Contain  ${Popup_Delate}  ${PopupDelate}
                                            Wait Until Element Is Visible  //div[@class='modal-body']/div/div/p  10s
                                            ${PopupDelate1}  Get Text  //div[@class='modal-body']/div/div/p
                                            Should Contain  ${Popup_Delate1}  ${PopupDelate1}
                                            Click Button  //div[@class='modal-body']/div/div[2]/button[2]
                                            Wait Until Page Contains  Your LOA was deleted.  10s
                                                Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT90-Pass.png
                                                ${wb}      Load Workbook     ${CURDIR}/${excel}
                                                Log to Console   ${wb}
                                                ${ws}      Set Variable  ${wb['Sheet1']}
                                                Log To Console   ${ws}
                                                Evaluate   $ws.cell(93,13,'PASS')
                                                Evaluate   $wb.save('${excel}')
                                                Close Browser
                                            
                                     ELSE
                                        Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT90-Notpass.png
                                        ${wb}      Load Workbook     ${CURDIR}/${excel}
                                        Log to Console   ${wb}
                                        ${ws}      Set Variable  ${wb['Sheet1']}
                                        Log To Console   ${ws}
                                        Evaluate   $ws.cell(93,13,'NOT PASS')
                                        Evaluate   $wb.save('${excel}')
                                        Close Browser
                                     END
                                ELSE
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT90-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(93,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                                END
                    
    ELSE IF    ${CheckShowData} > ${NUMBER}
                   ${Name_LOAS}  Get Text  //tbody/tr[${n}]/td[3]
                                IF  '${Name_LOAS}' == '${LOA_NAME}(${Random Numbers})'
                                     ${Show}  Get Text  //tbody/tr[${n}]/td[6]/span
                                     IF  '${Show}' == 'Draft'
                                        ${Num_Delate}  Get Text  //tbody/tr[${n}]/td[2]
                                            Click Button  //tbody/tr[${n}]/td[7]/div/button[2]
                                            Wait Until Element Is Visible  //div[@class='modal-body']/div/div/h3  10s
                                            ${PopupDelate}  Get Text  //div[@class='modal-body']/div/div/h3
                                            Should Contain  ${Popup_Delate}  ${PopupDelate}
                                            Wait Until Element Is Visible  //div[@class='modal-body']/div/div/p  10s
                                            ${PopupDelate1}  Get Text  //div[@class='modal-body']/div/div/p
                                            Should Contain  ${Popup_Delate1}  ${PopupDelate1}
                                            Click Button  //div[@class='modal-body']/div/div[2]/button[2]
                                            Wait Until Page Contains  Your LOA was deleted.  10s
                                                Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT90-Pass.png
                                                ${wb}      Load Workbook     ${CURDIR}/${excel}
                                                Log to Console   ${wb}
                                                ${ws}      Set Variable  ${wb['Sheet1']}
                                                Log To Console   ${ws}
                                                Evaluate   $ws.cell(93,13,'PASS')
                                                Evaluate   $wb.save('${excel}')
                                                ${Exit}  Set Variable  Exit
                                                Set Global Variable  ${Exit}
                                                Exit For Loop IF  "${Exit}" == "Exit" 
                                ELSE
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT90-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(93,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                            END
                        Exit For Loop IF  "${Exit}" == "Exit"  
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT90-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(93,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          
                    END
             Exit For Loop IF  "${Exit}" == "Exit"  
         END
          Exit For Loop IF  "${Exit}" == "Exit"  
    END 
        Exit For Loop IF  "${Exit}" == "Exit"  
        Run Keyword and Ignore Error   Click Element  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[text()='${Zn}']
    END 
    Close Browser
    END



*** Test Cases ***
Designer Approvers - CreateLOA_SaveDelate
    Open Website SMARTFLOW
    Requester - Auditor
    Click Btn Login 
    Input OTP And Click OTP 
    Click Nav LOA - Create LOA
    Create LOA - Tnput Data (2)
    Choose Manage Approver (2)
    Click btn Save 
    Check Create LOA 
    
    
    
  
    
    
   
    
    

    
    
    