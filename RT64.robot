*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot
Resource    functions/Quick Approve.robot

*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1
${InputText}  Please type the reason
*** Keywords ***


Click Btn Reject
    Click Button  //div[@class='text-center mt-1']/button[3]
    ${Reject}  Get Text  //div[@class='modal-body pt-0']/div/div/h3
    Should Contain  Reject  ${Reject}
    Input Text  //div[@class='modal-body pt-0']/div/div[2]/textarea  Please type the reason
    Click Button  //div[@class='modal-footer border-top-0 d-block pt-0']/div/button
    Wait Until Page Contains  Your document was rejected.  10s
    Sleep  5
     Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT64-Pass.png
    ${wb}      Load Workbook     ${CURDIR}/${excel}
    Log to Console   ${wb}
    ${ws}      Set Variable  ${wb['Sheet1']}
    Log To Console   ${ws}
    Evaluate   $ws.cell(67,13,'PASS')
    Evaluate   $wb.save('${excel}')
    Close Browser


Check Status Reject
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Click Button  //div[@class='col-sm-12 search-box-section']/button[2]
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button[2]  10s
    Input Text  //div[@class='col-sm-12 search-box-section']/input  ${NameDocument}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    IF    '${Show}' == 'No matching records found'
        Capture Page Screenshot   E:/Smartflow robot/Screenshor_Notpass/RT64-Notpass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(67,13,'NOT PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser 
    ELSE
    ${CountShowitems} =	Get Element Count  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a
    ${countAll}  Get Text  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[${CountShowitems}]
    ${CheckShowData} =	Get Element Count  //tbody/tr
    Log To Console  ${CheckShowData}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    FOR    ${z}    IN RANGE    ${countAll}
    ${Zn}  Evaluate  ${z}+1
    FOR    ${i}    IN RANGE    ${CheckShowData}
     ${n}  Evaluate  ${i}+1
     IF    ${CheckShowData} == ${NUMBER}
                                ${Show}  Get Text  //tbody/tr[1]/td[1]/button/p
                                IF  '${Show}' == '${NameDocument}'
                                    ${Show}  Get Text  //tbody/tr[1]/td[9]
                                    IF    '${Show}' == 'Rejected'
                                        Click Button  //tbody/tr[1]/td[10]/button
                                        Wait Until Element Is Visible  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/p/label/span  10s
                                        ${Status}  Get Text  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/p/label/span
                                        Should Contain      Rejected      ${Status}
                                        Wait Until Element Is Visible  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/p/label/button  10s
                                        Click Element  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/p/label/button
                                        ${MailReturn}  Get Text  //div[@class='modal-body']/div/h6
                                        Should Contain    Reject Reason :    ${MailReturn}
                                        ${TextReturn}  Get Text  //div[@class='modal-body']/div/p
                                        Should Contain    ${InputText}    ${TextReturn}
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT64-Pass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(67,13,'PASS')
                                    Evaluate   $wb.save('${excel}')
                                    ${Exit}  Set Variable  Exit
                                    Set Global Variable  ${Exit}
                                    Exit For Loop IF  "${Exit}" == "Exit" 
                                        Close Browser
                                    ELSE 
                                        Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT64-Notpass.png
                                        ${wb}      Load Workbook     ${CURDIR}/${excel}
                                        Log to Console   ${wb}
                                        ${ws}      Set Variable  ${wb['Sheet1']}
                                        Log To Console   ${ws}
                                        Evaluate   $ws.cell(67,13,'NOT PASS')
                                        Evaluate   $wb.save('${excel}') 
                                        Close Browser
                                    END
                                ELSE
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT64-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(67,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                                END
    ELSE IF    ${CheckShowData} > ${NUMBER}
                                ${Show}  Get Text  //tbody/tr[${n}]/td[1]/button/p
                                IF  '${Show}' == '${NameDocument}'
                                     ${Show}  Get Text  //tbody/tr[${n}]/td[9]
                                     IF    '${Show}' == 'Rejected'
                                        Click Button  //tbody/tr[1]/td[10]/button
                                        Wait Until Element Is Visible  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/p/label/span  10s
                                        ${Status}  Get Text  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/p/label/span
                                        Should Contain      Rejected      ${Status}
                                        Wait Until Element Is Visible  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/p/label/button  10s
                                        Click Element  //nav[@class='navbar navbar-expand navbar-document']/div/div/div/p/label/button
                                        ${MailReturn}  Get Text  //div[@class='modal-body']/div/h6
                                        Should Contain    Reject Reason :    ${MailReturn}
                                        ${TextReturn}  Get Text  //div[@class='modal-body']/div/p
                                        Should Contain    ${InputText}    ${TextReturn}
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT64-Pass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(67,13,'PASS')
                                    Evaluate   $wb.save('${excel}')
                                    ${Exit}  Set Variable  Exit
                                    Set Global Variable  ${Exit}
                                    Exit For Loop IF  "${Exit}" == "Exit"  
                                ELSE
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT64-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(67,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                            END
                        Exit For Loop IF  "${Exit}" == "Exit"  
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT64-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(67,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          
                    END
             Exit For Loop IF  "${Exit}" == "Exit"  
         END
          Exit For Loop IF  "${Exit}" == "Exit"  
    END 
        Exit For Loop IF  "${Exit}" == "Exit"  
        Run Keyword and Ignore Error   Click Element  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[text()='${Zn}']
    END 
    Close Browser
    END



*** Test Cases ***
WAITING FOR MY APPROVAL_QuickApprove-Reject
    Open Website SMARTFLOW
    Approver Login (MK)
    Click Btn Login 
    Input OTP And Click OTP
    Click btn Quick Approve
    Click Btn Reject
    

    
    

    
    
    