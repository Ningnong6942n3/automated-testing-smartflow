*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1
*** Keywords ***



Filter Doc No.
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Click Button  //div[@class='col-sm-12 search-box-section']/button[1]
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button[1]  10s
    ${Data2}  Get Text  //tbody/tr[1]/td[7]/div/button
    Log To Console   ${Data2}
    Input text  //div[@class='card table-filter']/div/div[4]/input  ${Data2}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    IF    '${Show}' == 'No matching records found'
        Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT54-Pass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(57,13,'PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser 
    ELSE
    ${count} =	Get Element Count  //tbody/tr
    Log To Console  ${count}
     IF    ${count} == ${NUMBER}
       ${Data}  Get Text  //tbody/tr[1]/td[7]/div/button  
                    IF   '${Data2}' == '${Data}'
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT54-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(57,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser 
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT54-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(57,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser 
                    END
    ELSE IF    ${count} > ${NUMBER}
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT54-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(57,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser 
    END 
    END

*** Test Cases ***
WAITING FOR MY APPROVAL_FilterDocNo
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP 
    Filter Doc No.
    
    
    
   
    