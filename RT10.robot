*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

*** Variables ***
${CreateDoc}  https://shl-dev.brainergy.digital/work-space/documents/my-document/create/content
${excel}   SmartFlow.xlsx
*** Keywords ***

Check Accuracy
    Wait Until Location Is Not  ${CreateDocumenturl}
    ${Nowurl}=   Get Location 
    IF    '${Nowurl}' == '${CreateDoc}'
              Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT10-Pass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(13,13,'PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
          ELSE
              Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT10-Notpass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(13,13,'NOT PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
    END
           
*** Test Cases ***
Document Details-Pass.
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Click Btn Create Document 
    Choose For
    Input Subject 
    Choose LOA 
    Choose Approval 1
    Choose Approval 2
    Click Btn Next
    Check Accuracy
    
    
    