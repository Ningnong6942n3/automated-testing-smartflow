*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot


*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1
${Document}  ทดสอบการอนุมัติเอกสาร
${TimeCreateRequester}
${TimeCreateApprove1}
${TimeCreateApprove2}
${TimeCreateRequester}
${TimeCreateFinal}
${Exit}
*** Keywords ***


Input Subject (Random Numbers)
    ${Random Numbers}  generate random string  3  [NUMBERS]
    Set Global Variable  ${Random Numbers}
    Input Text  //div[@class="col-sm-9"]/input  ${Document}(${Random Numbers})

Click btn Send (LC)
    Click Button  //div[@class='text-center p-3']/button[4]
    wait until location is  ${MAINWEB_Requester}  10s
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${NameDocument}  Get Text  //tbody/tr[1]/td[1]/button/p
    Should Contain  ${Document}(${Random Numbers})  ${NameDocument}
    ${Time}  Get Current Date  result_format=%d/%m/%Y %I:%M %p
    ${TimeCreateRequester}=  Convert To String  ${Time}
    Log To Console  ${TimeCreateRequester}

Search Document
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button[2]  10s
    Sleep  5
    Click Button  //div[@class='col-sm-12 search-box-section']/button[2]
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/input  10s
    input Text  //div[@class='col-sm-12 search-box-section']/input  ${Document}(${Random Numbers})
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}  
    ${CheckShowData} =	Get Element Count  //tbody/tr
    Log To Console  ${CheckShowData}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    Log to Console   ${Show}
    FOR    ${i}    IN RANGE    ${CheckShowData}
     ${n}  Evaluate  ${i}+1
     IF    ${CheckShowData} == ${NUMBER}
          IF    '${Show}' == 'No matching records found'
            Log to Console   No matching records found
          ELSE IF  '${Show}' == '${Show}'
            ${Show}  Get Text  //tbody/tr[1]/td[1]/button/p
            Should Contain    ${Document}(${Random Numbers})    ${Show}  
          END
     ELSE IF    ${CheckShowData} > ${NUMBER}
       ${Show}  Get Text  //tbody/tr[${n}]/td[1]/button/p
       Should Contain    ${Document}(${Random Numbers})    ${Show}  
     END
    END
    
Click Btn Preview - Approve
    ${StatusData}  Get Text  //tbody/tr[1]/td[6]/div/button 
    ${StatusDataNow}  Fetch From Right  ${StatusData}  /
    Click Button  //tbody/tr[1]/td[9]/button
    wait until location is  https://shl-dev.brainergy.digital/work-space/documents/details/BNG2021%252F${StatusDataNow}  10s
    Wait Until Element Is Visible  //div[@class='right-wrapper d-sm-none d-md-block d-none']/div[@class='right-container']/property-nav/div[@class='h-100 d-sm-none d-md-flex d-none nav-property']/ul[1]/li[3]  20s
    Click Element  //div[@class='right-wrapper d-sm-none d-md-block d-none']/div[@class='right-container']/property-nav/div[@class='h-100 d-sm-none d-md-flex d-none nav-property']/ul[1]/li[3]
Check Audit trail - Requester
    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[1]  10s
    ${NameRequester}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[1]
    Log To Console  ${NameRequester}
    Should Contain  Waritsara Sonchan  ${NameRequester}
    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[2]  10s
    ${ActionRequester}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[2]
    Log To Console  ${ActionRequester} 
    Should Contain  Create this document  ${ActionRequester} 
    Page Should Contain  ${TimeCreateRequester}
    
    
    

Check Audit trail - Approve 2
    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[2]/p[1]  10s
    ${NameRequester}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[2]/p[1]
    Log To Console  ${NameRequester}
    Should Contain  Waritsara Sonchan  ${NameRequester}
    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[2]/p[2]  10s
    ${ActionRequester}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[2]/p[2]
    Log To Console  ${ActionRequester} 
    Should Contain  Create this document  ${ActionRequester} 
    Page Should Contain  ${TimeCreateRequester}
    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[1]  10s
    ${NameApprove1}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[1]
    Log To Console  ${NameApprove1}
    Should Contain  Darin Pongsupat  ${NameApprove1}
    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[2]  10s
    ${ActionApprove1}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[2]
    Log To Console  ${ActionApprove1}
    Should Contain  Approve this document  ${ActionApprove1}
    Page Should Contain  ${TimeCreateApprove1}
Check Audit trail - Approve 3
    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[3]/p[1]  10s
    ${NameRequester}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[3]/p[1]
    Log To Console  ${NameRequester}
    Should Contain  Waritsara Sonchan  ${NameRequester}
    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[3]/p[2]  10s
    ${ActionRequester}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[3]/p[2]
    Log To Console  ${ActionRequester} 
    Should Contain  Create this document  ${ActionRequester} 
    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[2]/p[1]  10s
    ${NameApprove1}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[2]/p[1]
    Log To Console  ${NameApprove1}
    Should Contain  Darin Pongsupat  ${NameApprove1}
    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[2]/p[2]  10s
    ${ActionApprove1}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[2]/p[2]
    Log To Console  ${ActionApprove1}
    Should Contain  Approve this document  ${ActionApprove1}
    Page Should Contain  ${TimeCreateApprove1}
    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[1]  10s
    ${NameApprove2}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[1]
    Log To Console  ${NameApprove2}
    Should Contain  Mongkol Mulyongsri  ${NameApprove2}
    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[2]  10s
    ${ActionApprove2}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[2]
    Log To Console  ${ActionApprove2}
    Should Contain  Approve this document  ${ActionApprove2}
    Page Should Contain  ${TimeCreateApprove2}
    

Click Btn - Approve1 
    Click Button  //div[@class='text-center']/button[4]
    Wait Until Page Contains  Your document was approved and sent to the next level.  10s
    ${Time}  Get Current Date  result_format=%d/%m/%Y %I:%M %p
    ${TimeCreateApprove1}=  Convert To String  ${Time}
    Log To Console  ${TimeCreateApprove1}
    Wait until location is  https://shl-dev.brainergy.digital/work-space/documents/waiting-for-my-approval/tab/waiting-for-my-approval  10s

Click Btn - Approve2
    Click Button  //div[@class='text-center']/button[4]
    Wait Until Page Contains  Your document was approved and sent to the next level.  10s
    ${Time}  Get Current Date  result_format=%d/%m/%Y %I:%M %p
    ${TimeCreateApprove2}=  Convert To String  ${Time}
    Log To Console  ${TimeCreateApprove2}
    Wait until location is  https://shl-dev.brainergy.digital/work-space/documents/waiting-for-my-approval/tab/waiting-for-my-approval  10s
Click Btn Preview - Approve Final
    Click Button  //div[@class='text-center']/button[4]
    Wait Until Page Contains  Your document was approved  25s
    ${Time}  Get Current Date  result_format=%d/%m/%Y %I:%M %p
    ${TimeCreateFinal}=  Convert To String  ${Time}
    Log To Console  ${TimeCreateFinal}
    Wait until location is  https://shl-dev.brainergy.digital/work-space/documents/waiting-for-my-approval/tab/waiting-for-my-approval  10s

Search Document Final 
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Click Element  //div[@class]/div[@class='h-100']/app-waiting-for-my-approval/ul/li[2]
    Wait until location is  https://shl-dev.brainergy.digital/work-space/documents/waiting-for-my-approval/tab/my-approval  10s
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button[2]  10s
    Sleep  5
    Click Button  //div[@class='col-sm-12 search-box-section']/button[2]
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/input  10s
    input Text  //div[@class='col-sm-12 search-box-section']/input  ${Document}(${Random Numbers})
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}  
    ${Show}  Get Text  //tbody/tr/td
    IF    '${Show}' == 'No matching records found'
        Capture Page Screenshot   E:/Smartflow robot/Screenshor_Notpass/RT59-Notpass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(62,13,'NOT PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser 
    ELSE
    ${CountShowitems} =	Get Element Count  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a
    ${countAll}  Get Text  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[${CountShowitems}]
    ${CheckShowData} =	Get Element Count  //tbody/tr
    Log To Console  ${CheckShowData}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    FOR    ${z}    IN RANGE    ${countAll}
    ${Zn}  Evaluate  ${z}+1
    FOR    ${i}    IN RANGE    ${CheckShowData}
     ${n}  Evaluate  ${i}+1
     IF    ${CheckShowData} == ${NUMBER}
       ${Show}  Get Text  //tbody/tr[1]/td[1]/button/p
                    IF   '${Show}' == '${Document}(${Random Numbers})'
                          ${Show}  Get Text  //tbody/tr[1]/td[9]/button
                                IF   '${Show}' == 'Completed'
                                    Click Button  //tbody/tr[1]/td[10]/button
                                    Wait Until Element Is Visible  //div[@class='right-container']/property-nav/div[@class='h-100 d-sm-none d-md-flex d-none nav-property']/ul/li[3]/a[@class='text-center nav-link']  10s
                                    Click Element  //div[@class='right-container']/property-nav/div[@class='h-100 d-sm-none d-md-flex d-none nav-property']/ul/li[3]/a[@class='text-center nav-link']
                                    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[4]/p[1]  10s
                                    ${NameRequester}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[4]/p[1]
                                    Log To Console  ${NameRequester}
                                    Should Contain  Waritsara Sonchan  ${NameRequester}
                                    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[4]/p[2]  10s
                                    ${ActionRequester}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[4]/p[2]
                                    Log To Console  ${ActionRequester} 
                                    Should Contain  Create this document  ${ActionRequester} 
                                    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[3]/p[1]  10s
                                    ${NameApprove1}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[3]/p[1]
                                    Log To Console  ${NameApprove1}
                                    Should Contain  Darin Pongsupat  ${NameApprove1}
                                    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[3]/p[2]  10s
                                    ${ActionApprove1}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[3]/p[2]
                                    Log To Console  ${ActionApprove1}
                                    Should Contain  Approve this document  ${ActionApprove1}
                                    Page Should Contain  ${TimeCreateApprove1}
                                    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[2]/p[1]  10s
                                    ${NameApprove2}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[2]/p[1]
                                    Log To Console  ${NameApprove2}
                                    Should Contain  Mongkol Mulyongsri  ${NameApprove2}
                                    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[2]/p[2]  10s
                                    ${ActionApprove2}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[2]/p[2]
                                    Log To Console  ${ActionApprove2}
                                    Should Contain  Approve this document  ${ActionApprove2}
                                    Page Should Contain  ${TimeCreateApprove2}
                                    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[1]  10s
                                    ${NameApproveFinal}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[1]
                                    Log To Console  ${NameApproveFinal}
                                    Should Contain  Pakasit Wattana  ${NameApproveFinal}
                                    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[2]  10s
                                    ${ActionApproveFinal}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[2]
                                    Log To Console  ${ActionApproveFinal}
                                    Should Contain  Approve this document  ${ActionApproveFinal}
                                    Page Should Contain  ${TimeCreateFinal}
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT59-Pass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(62,13,'PASS')
                                    Evaluate   $wb.save('${excel}')
                                    Close Browser 
                                ELSE
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT59-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(62,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                                    Close Browser 
                                END
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT59-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(62,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser
                    END
    ELSE IF    ${CheckShowData} > ${NUMBER}
                    ${Show}  Get Text  //tbody/tr[${n}]/td[1]/button/p
                    IF    '${Show}' == '${Document}(${Random Numbers})'
                           ${Show}  Get Text  //tbody/tr[1]/td[9]/button
                             IF   '${Show}' == 'Completed'
                                    Click Button  //tbody/tr[1]/td[10]/button
                                    Wait Until Element Is Visible  //div[@class='right-container']/property-nav/div[@class='h-100 d-sm-none d-md-flex d-none nav-property']/ul/li[3]/a[@class='text-center nav-link']  10s
                                    Click Element  //div[@class='right-container']/property-nav/div[@class='h-100 d-sm-none d-md-flex d-none nav-property']/ul/li[3]/a[@class='text-center nav-link']
                                    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[4]/p[1]  10s
                                    ${NameRequester}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[4]/p[1]
                                    Log To Console  ${NameRequester}
                                    Should Contain  Waritsara Sonchan  ${NameRequester}
                                    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[4]/p[2]  10s
                                    ${ActionRequester}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[4]/p[2]
                                    Log To Console  ${ActionRequester} 
                                    Should Contain  Create this document  ${ActionRequester} 
                                    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[3]/p[1]  10s
                                    ${NameApprove1}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[3]/p[1]
                                    Log To Console  ${NameApprove1}
                                    Should Contain  Darin Pongsupat  ${NameApprove1}
                                    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[3]/p[2]  10s
                                    ${ActionApprove1}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[3]/p[2]
                                    Log To Console  ${ActionApprove1}
                                    Should Contain  Approve this document  ${ActionApprove1}
                                    Page Should Contain  ${TimeCreateApprove1}
                                    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[2]/p[1]  10s
                                    ${NameApprove2}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[2]/p[1]
                                    Log To Console  ${NameApprove2}
                                    Should Contain  Mongkol Mulyongsri  ${NameApprove2}
                                    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[2]/p[2]  10s
                                    ${ActionApprove2}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[2]/p[2]
                                    Log To Console  ${ActionApprove2}
                                    Should Contain  Approve this document  ${ActionApprove2}
                                    Page Should Contain  ${TimeCreateApprove2}
                                    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[1]  10s
                                    ${NameApproveFinal}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[1]
                                    Log To Console  ${NameApproveFinal}
                                    Should Contain  Pakasit Wattana  ${NameApproveFinal}
                                    Wait Until Element Is Visible  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[2]  10s
                                    ${ActionApproveFinal}  Get Text  //div[@class='tab-pane active']/property-audit-trail/div[@class='container-fluid m-0']/div[2]/ul/li[1]/p[2]
                                    Log To Console  ${ActionApproveFinal}
                                    Should Contain  Approve this document  ${ActionApproveFinal}
                                    Page Should Contain  ${TimeCreateFinal}
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT59-Pass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(62,13,'PASS')
                                    Evaluate   $wb.save('${excel}')
                                    ${Exit}  Set Variable  Exit
                                    Set Global Variable  ${Exit}
                                    Exit For Loop IF  "${Exit}" == "Exit"  
                                ELSE
                                    Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT59-Notpass.png
                                    ${wb}      Load Workbook     ${CURDIR}/${excel}
                                    Log to Console   ${wb}
                                    ${ws}      Set Variable  ${wb['Sheet1']}
                                    Log To Console   ${ws}
                                    Evaluate   $ws.cell(62,13,'NOT PASS')
                                    Evaluate   $wb.save('${excel}')
                            END
                        Exit For Loop IF  "${Exit}" == "Exit"  
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT49-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(52,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          
                    END
             Exit For Loop IF  "${Exit}" == "Exit"  
         END
          Exit For Loop IF  "${Exit}" == "Exit"  
    END 
        Exit For Loop IF  "${Exit}" == "Exit"  
        Run Keyword and Ignore Error   Click Element  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[text()='${Zn}']
    END 
    Close Browser
    END

 


*** Test Cases ***
Test_All_Approval
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Click Btn Create Document 
    Choose For
    Input Subject (Random Numbers)
    Choose LOA 
    Choose Approval 1
    Choose Approval 2
    Click Btn Next
    Input Article
    Click btn Send (LC)
    Sign Out 
    Approver Login (DR) 
    Click Btn Login 
    Input OTP And Click OTP
    Search Document
    Click Btn Preview - Approve
    Check Audit trail - Requester
    Click Btn - Approve1
    Sign Out 
    Approver Login (MK) 
    Click Btn Login 
    Input OTP And Click OTP
    Search Document
    Click Btn Preview - Approve
    Check Audit trail - Approve 2
    Click Btn - Approve2
    Sign Out 
    Approver Login (PS) 
    Click Btn Login 
    Input OTP And Click OTP
    Search Document
    Click Btn Preview - Approve
    Check Audit trail - Approve 3
    Click Btn Preview - Approve Final
    Search Document Final 
    
    
    
    
  
    
    

    
    
    