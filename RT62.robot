*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

*** Variables ***
${CommentBox}  //div[@class='right-wrapper d-sm-none d-md-block d-none']/div/property-nav/div/div/div/property-comment/div/div[2]/div[2]/form[@class='ng-pristine ng-invalid ng-touched']/textarea[1]
${CommentText}  ทดสอบการ Comment เอกสาร
${excel}   SmartFlow.xlsx 
${NUMBER}  1
${Exit}
*** Keywords ***


Click Btn Preview - Comment
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${StatusData}  Get Text  //tbody/tr[4]/td[6]/div/button 
    ${StatusDataNow}  Fetch From Right  ${StatusData}  /
    Click Button  //tbody/tr[4]/td[9]/button
    wait until location is  https://shl-dev.brainergy.digital/work-space/documents/details/BNG2021%252F${StatusDataNow}  10s
    Sleep  2
    Input Text  //*[@id="ngb-nav-10-panel"]/property-comment/div/div[2]/div[2]/form/textarea  ${CommentText}
    Click Button  //*[@id="ngb-nav-10-panel"]/property-comment/div/div[2]/div[2]/form/button
    ${Time}  Get Current Date  result_format= %I:%M %p
    ${TimeComment}=  Convert To String  ${Time}
    Log To Console  ${TimeComment}
    Wait Until Element Is Visible  //div[@class='d-flex flex-row']/div[2]/div/property-nav/div[1]/div/div/property-comment/div/div[2]/div/ul/li/div[@class='p-0 m-0 flex-column w-100']  10s
    ${CheckShowData} =	Get Element Count  //div[@class='d-flex flex-row']/div[2]/div/property-nav/div[1]/div/div/property-comment/div/div[2]/div/ul/li/div[@class='p-0 m-0 flex-column w-100']
    Log To Console  ${CheckShowData}
     IF  '${CheckShowData}' == '${NUMBER}'
       ${Comment}  Get Text  //div[@class='d-flex flex-row']/div[2]/div/property-nav/div[1]/div/div/property-comment/div/div[2]/div[@class='d-block comment-container']/ul/li[1]/div[2]/div[1]
        Log To Console    ${Comment}
                                    IF  '${CommentText}' == '${Comment}'
                                            Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT62-Pass.png
                                            ${wb}      Load Workbook     ${CURDIR}/${excel}
                                            Log to Console   ${wb}
                                            ${ws}      Set Variable  ${wb['Sheet1']}
                                            Log To Console   ${ws}
                                            Evaluate   $ws.cell(65,13,'PASS')
                                            Evaluate   $wb.save('${excel}')
                                            Close Browser
                                    ELSE
                                            Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT62-Notpass.png
                                            ${wb}      Load Workbook     ${CURDIR}/${excel}
                                            Log to Console   ${wb}
                                            ${ws}      Set Variable  ${wb['Sheet1']}
                                            Log To Console   ${ws}
                                            Evaluate   $ws.cell(65,13,'NOT PASS')
                                            Evaluate   $wb.save('${excel}')
                                            Close Browser
                                    END
    ELSE
        ${CheckShowData} =	Get Element Count  //div[@class='d-flex flex-row']/div[2]/div/property-nav/div[1]/div/div/property-comment/div/div[2]/div/ul/li/div[@class='p-0 m-0 flex-column w-100']
        Log to Console  ${CheckShowData}
        Sleep  5
        ${Comment}  Get Text  //div[@class='d-flex flex-row']/div[2]/div/property-nav/div[1]/div/div/property-comment/div/div[2]/div[@class='d-block comment-container']/ul/li[${CheckShowData}]/div[2]/div[1]
        Log To Console    ${Comment}
        IF  '${CommentText}' == '${Comment}'
            Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT62-Pass.png
            ${wb}      Load Workbook     ${CURDIR}/${excel}
            Log to Console   ${wb}
            ${ws}      Set Variable  ${wb['Sheet1']}
            Log To Console   ${ws}
            Evaluate   $ws.cell(65,13,'PASS')
            Evaluate   $wb.save('${excel}')
            Close Browser                
        ELSE
            Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT62-Notpass.png
            ${wb}      Load Workbook     ${CURDIR}/${excel}
            Log to Console   ${wb}
            ${ws}      Set Variable  ${wb['Sheet1']}
            Log To Console   ${ws}
            Evaluate   $ws.cell(65,13,'NOT PASS')
            Evaluate   $wb.save('${excel}')
            Close Browser
        END
               
    END                          
                    
                                    


*** Test Cases ***
WAITING FOR MY APPROVAL-Comment
    Open Website SMARTFLOW
    Approver Login (MK)
    Click Btn Login 
    Input OTP And Click OTP
    Click Btn Preview - Comment
    
    
    

    
    
    