*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

*** Variables ***

${excel}   SmartFlow.xlsx

*** Keywords ***


    
Click btn Edit
    Click Button  //div[@class='header-content']/button
    Sleep  2
    Click Element  //div[@class='container-fluid']/div[@class='row']/div[2]/div/div/button
    Sleep  2
    Click Element  //tbody/tr/td/span[text()='25']
    Click Button  //div[@class='text-center my-4']/button[2]
    Sleep  2
    

Check Change DocumentDetails    
    ${Message}  Get Text  //div[@class='row p-0 m-0']/div[@class='col-4 p-0']/div[@class='d-flex']/div[2]/p[text()='25/09/2021']
    IF    '${Message}' == '25/10/2021'
              Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT16-Pass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(19,13,'PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
          ELSE
              Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT16-Notpass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(19,13,'NOT PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
    END

*** Test Cases ***
CreateDocument-Edit3
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Click Btn Create Document 
    Choose For
    Input Subject 
    Choose LOA 
    Choose Approval 1
    Choose Approval 2
    Click Btn Next
    Click btn Edit
    Check Change DocumentDetails  
    

    
  
    
    
    