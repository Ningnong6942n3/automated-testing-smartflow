*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

*** Variables ***
${excel}   SmartFlow.xlsx
${text}  Pass 
${CheckShowData}
${NUMBER}  1
${Exit}
*** Keywords ***


Filter Filter-Status1
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Click Button  //div[@class='col-sm-12 search-box-section']/button[1]
    Wait Until Element Is Visible  //div[@class='card table-filter']/div/div  10s
    Click Element  //div[@class='card table-filter']/div/div[3]
    Wait Until Element Is Visible  //ng-dropdown-panel/div/div/div  10s
    Click Element  //ng-dropdown-panel/div/div/div[2]
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    IF    '${Show}' == 'No matching records found'
        Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT27-Pass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(30,13,'PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser 
    ELSE
    ${CountShowitems} =	Get Element Count  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a
    ${countAll}  Get Text  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[${CountShowitems}]
    ${CheckShowData} =	Get Element Count  //tbody/tr
    Log To Console  ${CheckShowData}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad} 
    ${Show}  Get Text  //tbody/tr/td
    FOR    ${z}    IN RANGE    ${countAll}
    ${Zn}  Evaluate  ${z}+1
    FOR    ${i}    IN RANGE    ${CheckShowData}
     ${n}  Evaluate  ${i}+1
     IF    ${CheckShowData} == ${NUMBER}
       ${Show}  Get Text  //tbody/tr[1]/td[9]/button     
                    IF   '${Show}' == 'Rejected'
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT27-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(30,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT27-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(30,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          ${Exit}  Set Variable  Exit
                          Set Global Variable  ${Exit}
                          Exit For Loop IF  "${Exit}" == "Exit"  
                    END
    ELSE IF    ${CheckShowData} > ${NUMBER}
                    ${Show}  Get Text  //tbody/tr[${n}]/td[9]/button
                    IF    '${Show}' == 'Rejected'
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT27-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(30,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT27-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(30,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          ${Exit}  Set Variable  Exit
                          Set Global Variable  ${Exit}
                          Exit For Loop IF  "${Exit}" == "Exit"  
                    END
         Exit For Loop IF  "${Exit}" == "Exit"  
         END
         Exit For Loop IF  "${Exit}" == "Exit"  
    END 
        Exit For Loop IF  "${Exit}" == "Exit"  
        Run Keyword and Ignore Error   Click Element  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[text()='${Zn}']
    END 
    Close Browser
    END
    



*** Test Cases ***
Filter-Status2
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP 
    Filter Filter-Status1
    
    