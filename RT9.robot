*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot

*** Variables ***
${excel}   SmartFlow.xlsx

*** Keywords ***


Click Btn OTP 
    Wait Until Element Is Visible  //div[@class='modal-body']/div/div/p
    ${OTPErrorMassage}  Get Text  //div[@class='modal-body']/div/div/p
    IF    '${OTPErrorMassage}' == 'OTP Is not valid!'
              Page Should Contain  OTP Is not valid!
              Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT9-Pass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(12,13,'PASS')
              Evaluate   $wb.save('${excel}')
              Close Browser
          ELSE
              Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT9-Notpass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(12,13,'NOT PASS')
              Evaluate   $wb.save('${excel}')
              Close Browser
    END
   

*** Test Cases ***
Test LOGIN PASS
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP Entered Incorrectly
    Click Btn OTP 
    