*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

*** Variables ***
${URL}  https://shl-dev.brainergy.digital/accounts/login
${OTP}  https://shl-dev.brainergy.digital/accounts/otp-confirm
${MAINWEB}  https://shl-dev.brainergy.digital/work-space/documents/my-document/tab/my-document
${CreateDoc}  https://shl-dev.brainergy.digital/work-space/documents/my-document/create/content
${BROWSER}  Chrome
${excel}   SmartFlow.xlsx
${text}  Pass 
*** Keywords ***

    
Click btn Edit
    Wait Until Element Is Visible  //div[@class='header-content']/button  10s
    Click Button  //div[@class='header-content']/button
    Wait Until Element Is Visible  //div[@class='col-sm']/ng-select[@id='reason']  10s
    Click Element  //div[@class='col-sm']/ng-select[@id='reason']
    Wait Until Element Is Visible  //ng-dropdown-panel/div/div/div[2]  10s
    Click Element  //ng-dropdown-panel/div/div/div[2]
    Wait Until Element Is Visible  //div[@class='text-center my-4']/button[2]  10s
    Click Button  //div[@class='text-center my-4']/button[2]


Check Change DocumentDetails    
    ${Message}  Get Text  //div[@class='col-4 p-0']/div/div/p[text()='เพื่อพิจารณา']
    IF    '${Message}' == 'เพื่อพิจารณา'
              Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT14-Pass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(17,13,'PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
          ELSE
              Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT14-Notpass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(17,13,'NOT PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
    END


*** Test Cases ***
CreateDocument-Edit1
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Click Btn Create Document 
    Choose For
    Input Subject 
    Choose LOA 
    Choose Approval 1
    Choose Approval 2
    Click Btn Next
    Click btn Edit
    Check Change DocumentDetails  
    
    
    
    
    