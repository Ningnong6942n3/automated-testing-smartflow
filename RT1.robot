*** Settings ***
Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot

*** Variables ***
${excel}   SmartFlow.xlsx
*** Keywords ***

Check URL Before Click OTP (Requestor)
    Wait Until Location Is  ${MAINWEB}
    ${Nowurl}=   Get Location 
    Log To Console  ${Nowurl}
    IF    '${Nowurl}' == '${MAINWEB}'
              Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT1-Pass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(4,13,'PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
          ELSE
              Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT1-Notpass.png
              ${wb}      Load Workbook     ${CURDIR}/${excel}
              Log to Console   ${wb}
              ${ws}      Set Variable  ${wb['Sheet1']}
              Log To Console   ${ws}
              Evaluate   $ws.cell(4,13,'NOT PASS')
              Evaluate   $wb.save('${excel}')
              Close All Browsers
    END
           
*** Test Cases ***
Test LOGIN PASS
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Check URL Before Click OTP (Requestor)
    