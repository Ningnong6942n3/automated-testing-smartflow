*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

*** Variables ***
${excel}   SmartFlow.xlsx
${text}  Pass 
${NUMBER}  1
${Exit}  
*** Keywords ***


Filter Document Type 
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Click Button  //div[@class='col-sm-12 search-box-section']/button[1]
    Wait Until Element Is Visible  //div[@class='card table-filter']/div/div[1]/ng-select  10s
    Click Element  //div[@class='card table-filter']/div/div[1]/ng-select
    Click Element  //div[@class='ng-dropdown-panel-items scroll-host']/div/div 
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad} 
    ${Show}  Get Text  //tbody/tr/td
    IF    '${Show}' == 'No matching records found'
        Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT22-Pass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(31,13,'PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser 
    ELSE
    ${CountShowitems} =	Get Element Count  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a
    ${countAll}  Get Text  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[${CountShowitems}]
    ${CheckShowData} =	Get Element Count  //tbody/tr
    Log To Console  ${CheckShowData}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad} 
    ${Show}  Get Text  //tbody/tr/td
    FOR    ${z}    IN RANGE    ${countAll}
    ${Zn}  Evaluate  ${z}+1
    FOR    ${i}    IN RANGE    ${CheckShowData}
     ${n}  Evaluate  ${i}+1
     IF    ${CheckShowData} == ${NUMBER}
       ${Show}  Get Text  //tbody/tr[1]/td[2]/button     
                    IF   '${Show}' == 'Memo'
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT22-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(25,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT22-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(25,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          ${Exit}  Set Variable  Exit
                          Set Global Variable  ${Exit}
                          Exit For Loop IF  "${Exit}" == "Exit"  
                    END
    ELSE IF    ${CheckShowData} > ${NUMBER}
                    ${Show}  Get Text  //tbody/tr[${n}]/td[2]/button
                    IF    '${Show}' == 'Memo'
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT22-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(25,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT22-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(25,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          ${Exit}  Set Variable  Exit
                          Set Global Variable  ${Exit}
                          Exit For Loop IF  "${Exit}" == "Exit"  
                    END
         Exit For Loop IF  "${Exit}" == "Exit"  
         END
         Exit For Loop IF  "${Exit}" == "Exit"  
    END 
        Exit For Loop IF  "${Exit}" == "Exit"  
        Run Keyword and Ignore Error   Click Element  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[text()='${Zn}']
    END 
    Close Browser
    END
    
*** Test Cases ***
Filter - Type
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP
    Filter Document Type 
   
    