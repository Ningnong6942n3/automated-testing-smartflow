*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot


*** Variables ***
${URL}  https://shl-dev.brainergy.digital/accounts/login
${OTP}  https://shl-dev.brainergy.digital/accounts/otp-confirm
${MAINWEB}  https://shl-dev.brainergy.digital/work-space/documents/my-document/tab/my-document
${CreateDoc}  https://shl-dev.brainergy.digital/work-space/documents/my-document/create/content
${BROWSER}  Chrome
${excel}   SmartFlow.xlsx
${text}  Pass 
*** Keywords ***


Choose Tab User Manuals
    Wait Until Element Is Visible  //app-sidebar/nav/ul/li[4]  10s
    Click Element  //app-sidebar/nav/ul/li[4]
    wait until location is  ${UserManuals}  10s
    Wait Until Element Is Visible  //div[@class='user-manuals-wrapper']/ul/li[7]/div/button/span  10s
    ${UM}  Get Text  //div[@class='user-manuals-wrapper']/ul/li[7]/div/button/span
    Log to Console  ${UM}
    Click Element  //div[@class='user-manuals-wrapper']/ul/li[7]/div/button/span
    ${UserManuals}  Get Text  //div[@class='modal-body']/div/div/h3
    Log to Console  ${UserManuals}
    IF   '${UM}' == '${UserManuals}'
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT40-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(43,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser
     ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT40-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(43,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          Close Browser
     END

*** Test Cases ***
UserManuals
    Open Website SMARTFLOW
    Requestor Login 
    Click Btn Login (Requestor)
    Input OTP And Click OTP 
    Choose Tab User Manuals
    
    

    
    
    