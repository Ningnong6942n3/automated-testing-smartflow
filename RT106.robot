*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1
${ErrorPosition}  Please select Position
*** Keywords ***





Edit - Add Employees
    Wait Until Element Is Visible  //div[@class='form-btn-wrapper']/button[3]  10s
    Click Button  //div[@class='form-btn-wrapper']/button[3]
    Wait Until Element Is Visible  //div[@class='modal-body']/form/div/div[3]/button[1]  10s
    Click Button  //div[@class='modal-body']/form/div/div[3]/button[1]
    
Error Massage 
    ${ErrorMassage}  Get Text  //div[@class='d-flex flex-column']/div[1]/div/div/div/div
    IF  '${ErrorMassage}' == '${ErrorPosition}'
        Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT106-Pass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(109,13,'PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser
    ELSE  
        Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT106-Notpass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(109,13,'NOT PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser
    END



*** Test Cases ***
EditAddEmployees_Error1
    Open Website SMARTFLOW
    Requester - Auditor
    Click Btn Login 
    Input OTP And Click OTP 
    Click Nav LOA - Edit (Tab)
    Edit - Add Employees
    Error Massage 
   
    
    

    
    
    