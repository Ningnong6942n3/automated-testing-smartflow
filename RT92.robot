*** Settings ***

Library     Selenium2Library
Library     String
Library     openpyxl
Library     BuiltIn
Library     DateTime
Resource    functions/Login.robot
Resource    functions/CreateDocument .robot

Resource    functions/Create LOA.robot

*** Variables ***
${excel}   SmartFlow.xlsx
${NUMBER}  1
${PopUpCancle}  Are you sure to cancel?
${Employees_NA}
${Pocition_Approver1}  Department Director
${Pocition_Approver2}  Operation Manager
${LOA_NAME}  ทดสอบการค้นหาLOA
${LOACode}
${Exit}
*** Keywords ***



Click btn Save 
    Click Button  //div[@class='form-action-wrapper']/button[3]
    Wait Until Page Contains  The LOA was saved successfully  10s
    Wait Until Element Is Visible  //ul[@class='nav']/li[4]/div/ul/li[2]/a  10s
    Sleep  5 
    Click Element  //ul[@class='nav']/li[4]/div/ul/li[2]/a

Check Create LOA 
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/button[2]  10s
    Sleep  5
    Click Button  //div[@class='col-sm-12 search-box-section']/button[2]
    Wait Until Element Is Visible  //div[@class='col-sm-12 search-box-section']/input  10s
    input Text  //div[@class='col-sm-12 search-box-section']/input  ${LOA_NAME}(${Random Numbers})
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    IF    '${Show}' == 'No matching records found'
        Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT92-Pass.png
        ${wb}      Load Workbook     ${CURDIR}/${excel}
        Log to Console   ${wb}
        ${ws}      Set Variable  ${wb['Sheet1']}
        Log To Console   ${ws}
        Evaluate   $ws.cell(95,13,'PASS')
        Evaluate   $wb.save('${excel}')
        Close Browser 
    ELSE
    ${CountShowitems} =	Get Element Count  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a
    ${countAll}  Get Text  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[${CountShowitems}]
    ${CheckShowData} =	Get Element Count  //tbody/tr
    Log To Console  ${CheckShowData}
    Wait Until Element Is Visible  //tbody/tr/td  ${TimeLoad}
    ${Show}  Get Text  //tbody/tr/td
    FOR    ${z}    IN RANGE    ${countAll}
    ${Zn}  Evaluate  ${z}+1
    FOR    ${i}    IN RANGE    ${CheckShowData}
     ${n}  Evaluate  ${i}+1
     IF    ${CheckShowData} == ${NUMBER}
       ${Data}  Get Text  //tbody/tr[1]/td[3]    
                    IF   '${LOA_NAME}(${Random Numbers})' == '${Data}'
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT92-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(95,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT92-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(95,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          ${Exit}  Set Variable  Exit
                          Set Global Variable  ${Exit}
                          Exit For Loop IF  "${Exit}" == "Exit"  
                    END
    ELSE IF    ${CheckShowData} > ${NUMBER}
                    ${Data}  Get Text  //tbody/tr[${n}]/td[3]  
                    IF    '${LOA_NAME}(${Random Numbers})' == '${Data}'
                          Capture Page Screenshot  E:/Smartflow robot/Screenshot_Pass/RT92-Pass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(95,13,'PASS')
                          Evaluate   $wb.save('${excel}')
                    ELSE
                          Capture Page Screenshot  E:/Smartflow robot/Screenshor_Notpass/RT92-Notpass.png
                          ${wb}      Load Workbook     ${CURDIR}/${excel}
                          Log to Console   ${wb}
                          ${ws}      Set Variable  ${wb['Sheet1']}
                          Log To Console   ${ws}
                          Evaluate   $ws.cell(95,13,'NOT PASS')
                          Evaluate   $wb.save('${excel}')
                          ${Exit}  Set Variable  Exit
                          Set Global Variable  ${Exit}
                          Exit For Loop IF  "${Exit}" == "Exit"  
                    END
         Exit For Loop IF  "${Exit}" == "Exit"  
         END
         Exit For Loop IF  "${Exit}" == "Exit"  
    END 
        Exit For Loop IF  "${Exit}" == "Exit"  
        Run Keyword and Ignore Error   Click Element  //div[@class='col-sm-12 table-footer-paging']/div[2]/span/a[text()='${Zn}']
    END 
    Close Browser
    END



*** Test Cases ***
Designer Approvers - CreateLOA_Search
    Open Website SMARTFLOW
    Requester - Auditor
    Click Btn Login 
    Input OTP And Click OTP
    Click Nav LOA - Create LOA
    Create LOA - Tnput Data (2)
    Choose Manage Approver (2)
    Click btn Save 
    Check Create LOA 
    
    
    
  
    
    
   
    
    

    
    
    